import 'package:flutter/widgets.dart';

class SizeConfig {
  static MediaQueryData? _mediaQueryData;
  static double? screenWidth;
  static double? screenHeight;
  static double? bottomPadding;
  static double? blockSizeHorizontal;
  static double? blockSizeVertical;
  static double? topPadding;
  static double? tittle1;
  static double? body1;
  static double? foot1;
  static double? tittle2;
  static double? body2;
  static double? foot2;
  static double? tittle3;
  static double? body3;
  static double? foot3;
  static double? tittleVaxScreen;
  static double? bodyVaxScreen;
  static double? footVaxScreen;
  static double? formWidth;
  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    topPadding = _mediaQueryData!.padding.top;
    bottomPadding = _mediaQueryData!.padding.bottom;
    screenWidth = _mediaQueryData!.size.width;
    screenHeight = _mediaQueryData!.size.height - topPadding!;
    blockSizeHorizontal = screenWidth! / 100;
    blockSizeVertical = screenHeight! / 100;
    tittle1 = screenHeight! * 0.3;
    body1 = screenHeight! * 0.4;
    foot1 = screenHeight! * 0.3;
    tittle2 = screenHeight! * 0.15;
    body2 = screenHeight! * 0.8;
    foot2 = screenHeight! * 0.05;
    tittle3 = screenHeight! * 0.45;
    body3 = screenHeight! * 0.50;
    foot3 = screenHeight! * 0.05;
    tittleVaxScreen = screenHeight! * 0.42;
    bodyVaxScreen = screenHeight! * 0.46;
    footVaxScreen = screenHeight! * 0.12;
    formWidth = screenWidth! * 0.8;
  }
}
