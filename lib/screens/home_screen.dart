import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
//providers
import '../providers/user_provider.dart';
import '../providers/pet_provider.dart';
//constants
import '../constans.dart';
//models
import '../models/user_model.dart';
import '../models/pet_model.dart';
//constants
import '../size_config.dart';
//widgets
import '../widgets/made_by_mrgasha_text_btn.dart';
import '../widgets/load_indicator.dart';
import '../widgets/top_padding.dart';
import '../widgets/profile_pic.dart';
import '../widgets/tab_widget/tab_widget.dart';

class HomeScreen extends StatefulWidget {
  static const routName = 'home/';
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // bool _isInit = false;
  late Future<int> future;
  @override
  void initState() {
    super.initState();
    future = Provider.of<UserProvider>(context, listen: false)
        .getUserById()
        .then((_) {
      Provider.of<PetProvider>(context, listen: false).getPetsByUser();
      Provider.of<PetProvider>(context, listen: false).getRemindersByUser();
      return successfullRequest;
    });
  }

  @override
  Widget build(BuildContext context) {
    final AppLocalizations localizations = AppLocalizations.of(context)!;
    final ThemeData theme = Theme.of(context);
    final UserProvider userProvider = Provider.of<UserProvider>(context);

    SizeConfig().init(context);
    return FutureBuilder(
        future: future,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return Container(
              alignment: Alignment.center,
              height: SizeConfig.screenHeight,
              width: SizeConfig.screenWidth,
              child: Scaffold(
                  body: SingleChildScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const TopPadding(),
                    SizedBox(
                      width: double.infinity,
                      height: SizeConfig.tittle3!,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Text(
                            localizations.greeting(
                                '${userProvider.user?.name} ${userProvider.user?.lastName}'),
                            style: theme.textTheme.headlineLarge,
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: SizeConfig.blockSizeHorizontal! * 40,
                            width: SizeConfig.blockSizeHorizontal! * 40,
                            child: ProfilePic(
                              pic: userProvider.user?.profilePic,
                              picURL: userProvider.user?.profilePicURL,
                            ),
                          ),
                          Column(
                            children: <Widget>[
                              Text(
                                localizations.proud,
                                style: theme.textTheme.titleMedium!.copyWith(
                                  color: theme.primaryColor,
                                  fontWeight: FontWeight.w100,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                width: SizeConfig.blockSizeHorizontal! * 70,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    TextButton(
                                      onPressed: () {},
                                      child: Text(
                                        localizations.editProfile,
                                        style: theme.textTheme.labelLarge!
                                            .copyWith(
                                          color: theme.primaryColor,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    TextButton(
                                      onPressed: () {},
                                      child: Text(
                                        localizations.settings,
                                        style: theme.textTheme.labelLarge!
                                            .copyWith(
                                          color: theme.primaryColor,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.body3,
                      child: const TabWidget(),
                    ),
                    SizedBox(
                      height: SizeConfig.foot3,
                      child: const MadeByMrGasha(),
                    )
                  ],
                ),
              )),
            );
          } else if (snapshot.hasError) {
            return Container(
              alignment: Alignment.center,
              height: SizeConfig.screenHeight,
              width: SizeConfig.screenWidth,
              child: Scaffold(
                body: Center(
                  child: Text(
                    localizations.onError,
                    style: theme.textTheme.headlineSmall,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            );
          } else {
            return Container(
                alignment: Alignment.center,
                height: SizeConfig.screenHeight,
                width: SizeConfig.screenWidth,
                child: const Scaffold(
                    body: Center(
                  child: LoadIndicator(),
                )));
          }
        });
  }
}
