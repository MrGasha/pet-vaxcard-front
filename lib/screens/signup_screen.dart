import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:pet_vax_app/models/user_model.dart';
import 'package:pet_vax_app/providers/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
//constants
import '../constans.dart';
import '../errors.dart';
import '../size_config.dart';
//providers
import '../providers/validator_provider.dart';
//widgets
import '../widgets/custom_dropdown_form.dart';
import '../widgets/top_padding.dart';
import '../widgets/profile_pic.dart';
import '../widgets/camera_icon_btn.dart';
import '../widgets/load_indicator.dart';
import '../widgets/made_by_mrgasha_text_btn.dart';

class SignUpScreen extends StatefulWidget {
  static const routeName = '/signUp';
  const SignUpScreen({super.key});

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _formKey = GlobalKey<FormState>();
  final FocusNode _emailFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();
  final FocusNode _passwordConfirmFocusNode = FocusNode();
  final FocusNode _nameFocusNode = FocusNode();
  final FocusNode _lastNameFocusNode = FocusNode();
  //user initialization for better management
  User user = User(
    email: '',
    password: '',
    name: '',
    lastName: '',
    gender: 0,
    isVet: false,
    locationId: 0,
    birthdate: DateTime.now(),
  );

  bool isVet = false;

  @override
  void dispose() {
    _emailFocusNode.dispose();
    _passwordFocusNode.dispose();
    _passwordConfirmFocusNode.dispose();
    _nameFocusNode.dispose();
    _lastNameFocusNode.dispose();

    super.dispose();
  }

  //focus nodes to change the label on focused and to request Focus
  void _requestFocus(focusNode) {
    setState(() {
      FocusScope.of(context).requestFocus(focusNode);
    });
  }

  void onCheckBoxChanged() {
    setState(() {
      isVet = !isVet;
      user = user.copyWith(isVet: isVet);
    });
  }

  @override
  Widget build(BuildContext context) {
    final AppLocalizations localization = AppLocalizations.of(context)!;
    final ThemeData theme = Theme.of(context);
    final ValidatorProvider validatorProvider =
        Provider.of<ValidatorProvider>(context);
    final UserProvider userProvider = Provider.of<UserProvider>(context);
    final List<String> genderList = [
      localization.gender,
      localization.male,
      localization.female,
      localization.other,
    ];
    String dropdownValue = localization.gender;
    SizeConfig().init(context);
    //todo:
    //  Valdation
    void dropdownCallBack(String selectedValue) {
      setState(
        () {
          dropdownValue = selectedValue;

          user = user.copyWith(gender: genderList.indexOf(selectedValue));
        },
      );
    }

    void goHome() {
      if (userProvider.authUser != null) {
        Navigator.of(context).pop();
        //for going back to login screen and auto login
      }
    }

    return Container(
      width: SizeConfig.screenWidth,
      height: SizeConfig.screenHeight,
      alignment: Alignment.center,
      child: Scaffold(
        body: WillPopScope(
          onWillPop: () async {
            Navigator.pop(context);
            validatorProvider.wipeData();
            return false;
          },
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                const TopPadding(),
                SizedBox(
                  height: SizeConfig.tittle2,
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.topLeft,
                        child: IconButton(
                          icon: const Icon(
                            Icons.arrow_back,
                          ),
                          splashRadius: defaultIconSplashRadius,
                          onPressed: () => {
                            Navigator.of(context).pop(),
                            validatorProvider.wipeData(),
                          },
                        ),
                      ),
                      Text(
                        localization.signUp,
                        style: Theme.of(context).textTheme.headlineLarge,
                      ),
                    ],
                  ),
                ),
                ConstrainedBox(
                  constraints: BoxConstraints(minHeight: SizeConfig.body2!),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        SizedBox(
                          width: SizeConfig.formWidth,
                          child: Column(
                            children: <Widget>[
                              TextFormField(
                                //e-mail form
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                onSaved: (newValue) =>
                                    user = user.copyWith(email: newValue),
                                decoration: InputDecoration(
                                  labelText: localization.email,
                                  hintText: localization.emailHint,
                                  labelStyle: TextStyle(
                                    color: _emailFocusNode.hasFocus
                                        ? theme.primaryColor
                                        : theme.hintColor,
                                  ),
                                ),
                                validator: (value) {
                                  //there must be a better way to show localize errors with
                                  //providers
                                  switch (validatorProvider.emailValidator(
                                    value!,
                                  )) {
                                    case isEmpty:
                                      {
                                        return localization.isEmptyError;
                                      }
                                    case isNotValid:
                                      {
                                        return localization.invalidEmailError;
                                      }

                                    default:
                                      {
                                        //this if is isValid
                                        return null;
                                      }
                                  }
                                },

                                focusNode: _emailFocusNode,
                                onTap: () {
                                  _requestFocus(_emailFocusNode);
                                },
                                onFieldSubmitted: (_) {
                                  _requestFocus(_passwordFocusNode);
                                },
                              ),
                              TextFormField(
                                //Pasword Form
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                decoration: InputDecoration(
                                  labelText: localization.password,
                                  hintText: localization.passwordHint,
                                  labelStyle: TextStyle(
                                    color: _passwordFocusNode.hasFocus
                                        ? theme.focusColor
                                        : theme.hintColor,
                                  ),
                                ),
                                validator: (value) {
                                  validatorProvider.userPassword = value!;
                                  switch (validatorProvider.passwordValidator(
                                    value,
                                  )) {
                                    case isEmpty:
                                      {
                                        return localization.isEmptyError;
                                      }
                                    case isNotValid:
                                      {
                                        return localization
                                            .invalidPasswordError;
                                      }

                                    default:
                                      {
                                        //this if is isValid
                                        return null;
                                      }
                                  }
                                },
                                obscureText: true,
                                focusNode: _passwordFocusNode,
                                onTap: () => {
                                  _requestFocus(_passwordFocusNode),
                                },
                                onFieldSubmitted: (_) => {
                                  _requestFocus(_passwordConfirmFocusNode),
                                },
                              ),
                              TextFormField(
                                //passwordConfirm
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                onSaved: (newValue) => user = user.copyWith(
                                  password: newValue,
                                ),
                                decoration: InputDecoration(
                                  labelText: localization.passwordConfirm,
                                  hintText: localization.passwordConfirmHint,
                                  labelStyle: TextStyle(
                                    color: _passwordConfirmFocusNode.hasFocus
                                        ? theme.focusColor
                                        : theme.hintColor,
                                  ),
                                ),
                                obscureText: true,
                                validator: (value) {
                                  switch (validatorProvider
                                      .passwordconfirmValidator(
                                    value!,
                                  )) {
                                    case isEmpty:
                                      {
                                        return localization.isEmptyError;
                                      }
                                    case isNotValid:
                                      {
                                        return localization
                                            .invalidPasswordError;
                                      }
                                    case noMatchPassword:
                                      {
                                        return localization
                                            .passwordNotMatchingError;
                                      }
                                    default:
                                      {
                                        //this if is isValid
                                        return null;
                                      }
                                  }
                                },
                                focusNode: _passwordConfirmFocusNode,
                                onTap: () => {
                                  _requestFocus(_passwordConfirmFocusNode),
                                },
                                onFieldSubmitted: (_) => {
                                  _requestFocus(_nameFocusNode),
                                },
                              ),
                              TextFormField(
                                //Name
                                textCapitalization:
                                    TextCapitalization.sentences,
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                onSaved: (newValue) => user = user.copyWith(
                                  name: newValue,
                                ),
                                decoration: InputDecoration(
                                  labelText: localization.name,
                                  hintText: localization.nameHint,
                                  labelStyle: TextStyle(
                                    color: _nameFocusNode.hasFocus
                                        ? theme.focusColor
                                        : theme.hintColor,
                                  ),
                                ),
                                validator: (value) {
                                  switch (validatorProvider.namesValidator(
                                    value!,
                                  )) {
                                    case isEmpty:
                                      {
                                        return localization.isEmptyError;
                                      }
                                    case isNotValid:
                                      {
                                        return localization.invalidFieldError;
                                      }
                                    default:
                                      {
                                        //this if is isValid
                                        return null;
                                      }
                                  }
                                },
                                focusNode: _nameFocusNode,
                                onTap: () => {
                                  _requestFocus(_nameFocusNode),
                                },
                                onFieldSubmitted: (_) => {
                                  _requestFocus(_lastNameFocusNode),
                                },
                              ),
                              TextFormField(
                                //LastName
                                textCapitalization:
                                    TextCapitalization.sentences,
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                onSaved: (newValue) => user = user.copyWith(
                                  lastName: newValue,
                                ),
                                decoration: InputDecoration(
                                  labelText: localization.lastName,
                                  hintText: localization.lastNameHint,
                                  labelStyle: TextStyle(
                                    color: _lastNameFocusNode.hasFocus
                                        ? theme.focusColor
                                        : theme.hintColor,
                                  ),
                                ),
                                validator: (value) {
                                  switch (validatorProvider.namesValidator(
                                    value!,
                                  )) {
                                    case isEmpty:
                                      {
                                        return localization.isEmptyError;
                                      }
                                    case isNotValid:
                                      {
                                        return localization.invalidFieldError;
                                      }
                                    default:
                                      {
                                        //this if is isValid
                                        return null;
                                      }
                                  }
                                },
                                focusNode: _lastNameFocusNode,
                                onTap: () => {
                                  _requestFocus(_lastNameFocusNode),
                                },
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(localization.iAmVet,
                                      style: theme.textTheme.bodyMedium!),
                                  Checkbox(
                                    value: isVet,
                                    onChanged: (_) => {
                                      onCheckBoxChanged(),
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            SizedBox(
                              width: SizeConfig.blockSizeHorizontal! * 38,
                              child: Column(
                                children: <Widget>[
                                  SizedBox(
                                    height:
                                        SizeConfig.blockSizeHorizontal! * 38,
                                    child: ProfilePic(
                                      pic: validatorProvider.selectedImage,
                                    ),
                                  ),
                                  CameraIconButton(
                                    isSelected:
                                        validatorProvider.selectedImage != null,
                                    onPressed: validatorProvider.selectImage,
                                  )
                                ],
                              ),
                            ),
                            SizedBox(
                              width: SizeConfig.blockSizeHorizontal! * 55,
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Wrap(
                                    spacing: defaultPadding,
                                    alignment: WrapAlignment.spaceAround,
                                    children: <Widget>[
                                      Column(
                                        children: <Widget>[
                                          ElevatedButton(
                                            style: ButtonStyle(
                                              textStyle:
                                                  MaterialStateProperty.all(
                                                      theme.textTheme
                                                          .labelMedium),
                                            ),
                                            onPressed: () {
                                              validatorProvider
                                                  .getCalendar(context);
                                            },
                                            child: Text(
                                              validatorProvider.isDateConfirmed
                                                  ? DateFormat('dd/MM/yy')
                                                      .format(validatorProvider
                                                          .selectedDate)
                                                  : localization.birthDate,
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                          validatorProvider.isDateValid
                                              ? const SizedBox()
                                              : Text(
                                                  localization.noSelectionError,
                                                  overflow:
                                                      TextOverflow.visible,
                                                  style: theme
                                                      .textTheme.bodySmall!
                                                      .copyWith(
                                                    color: theme.errorColor,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                        ],
                                      ),
                                      Column(
                                        children: <Widget>[
                                          ElevatedButton(
                                            onPressed: () {
                                              validatorProvider
                                                  .onLocationPressed(context);
                                            },
                                            child: Text(
                                              validatorProvider.locationId ==
                                                      null
                                                  ? localization.location
                                                  : validatorProvider.cityList
                                                      .firstWhere(
                                                        (element) =>
                                                            element.cityId ==
                                                            validatorProvider
                                                                .locationId,
                                                      )
                                                      .cityName,
                                              style:
                                                  theme.textTheme.labelMedium,
                                            ),
                                          ),
                                          validatorProvider.isLocationValid
                                              ? const SizedBox()
                                              : Text(
                                                  localization.noSelectionError,
                                                  style: theme
                                                      .textTheme.bodySmall!
                                                      .copyWith(
                                                          color:
                                                              theme.errorColor,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  const Padding(
                                    padding:
                                        EdgeInsets.all(defaultPadding / 2.2),
                                  ),
                                  SizedBox(
                                    height: SizeConfig.blockSizeVertical! * 8,
                                    width: SizeConfig.blockSizeHorizontal! * 40,
                                    child: CustomDropDownForm(
                                      dropdownCallBack: (selectedValue) {
                                        dropdownCallBack(selectedValue);
                                      },
                                      dropdownItems: genderList,
                                      dropdownValue: dropdownValue,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        ElevatedButton(
                          onPressed: () async {
                            if (validatorProvider.isSignUpValid(_formKey)) {
                              showDialog(
                                context: context,
                                builder: (context) {
                                  return const LoadIndicator();
                                },
                              );
                              _formKey.currentState!.save();
                              user = user.copyWith(
                                  locationId: validatorProvider
                                      .locationId); //setting the location as this value currently is stored on provider
                              validatorProvider.selectedImage != null
                                  ? userProvider
                                      .signUp(
                                          user.copyWith(
                                            profilePic:
                                                validatorProvider.selectedImage,
                                          ),
                                          context)
                                      .then(
                                        (_) => {
                                          Navigator.of(context).pop(),
                                          goHome()
                                        },
                                      )
                                  : userProvider.signUp(user, context).then(
                                        (_) => {
                                          Navigator.of(context).pop(),
                                          goHome()
                                        },
                                      );
                            }
                          },
                          child: Text(
                            localization.signUp,
                            style: theme.textTheme.labelMedium,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.foot2,
                  child: const MadeByMrGasha(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
