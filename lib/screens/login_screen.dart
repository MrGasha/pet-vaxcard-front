import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

//constants
import '../constans.dart';
import '../errors.dart';
import '../size_config.dart';
import '../widgets/top_padding.dart';
//providers
import '../providers/user_provider.dart';
import '../providers/validator_provider.dart';
//models
import '../models/user_model.dart';
// screens
import './signup_screen.dart';
//widgets
import '../widgets/load_indicator.dart';
import '../widgets/made_by_mrgasha_text_btn.dart';

//Focus node approach for changing the label color seems weird but it seems is standar
//Setting sizedboxes for form distribution on screen seems wrong but works for now
class LoginScreen extends StatefulWidget {
  static const routeName = '/login';
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final FocusNode _emailFocusNode = FocusNode();
  final FocusNode _passwordFocusnode = FocusNode();
  User user = User();

  @override
  void dispose() {
    _emailFocusNode.dispose();
    _passwordFocusnode.dispose();
    super.dispose();
  }

  //focus nodes to change the label on focused and to request Focus
  void _requestFocus(focusNode) {
    setState(() {
      FocusScope.of(context).requestFocus(focusNode);
    });
  }

  @override
  Widget build(BuildContext context) {
    final localization = AppLocalizations.of(context)!;
    final theme = Theme.of(context);
    final UserProvider userProvider = Provider.of<UserProvider>(context);
    final ValidatorProvider validatorProvider =
        Provider.of<ValidatorProvider>(context);

    SizeConfig().init(context);

    return Container(
      width: SizeConfig.screenWidth,
      height: SizeConfig.screenHeight,
      alignment: Alignment.center,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const TopPadding(),
              SizedBox(
                height: SizeConfig.tittle1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(30.0),
                      child: Text(
                        "MyPet Card",
                        style: theme.textTheme.headlineLarge,
                      ),
                    ),
                    Image.asset('assets/images/mypet_logo.png'),
                  ],
                ),
              ),
              SizedBox(
                height: SizeConfig.body1,
                width: SizeConfig.formWidth,
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      TextFormField(
                        onTap: () => {
                          _requestFocus(_emailFocusNode),
                        },
                        decoration: InputDecoration(
                          labelText: localization.email,
                          hintText: localization.emailHint,
                          labelStyle: TextStyle(
                            color: _emailFocusNode.hasFocus
                                ? theme.primaryColor
                                : theme.hintColor,
                          ),
                        ),
                        onSaved: (newValue) => user =
                            user.copyWith(email: newValue!.toLowerCase()),
                        validator: (value) {
                          switch (validatorProvider.emailValidator(
                            value!,
                          )) {
                            case isEmpty:
                              {
                                return localization.isEmptyError;
                              }
                            case isNotValid:
                              {
                                return localization.invalidEmailError;
                              }

                            default:
                              {
                                //this if is isValid
                                return null;
                              }
                          }
                        },
                        focusNode: _emailFocusNode,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.emailAddress,
                        onFieldSubmitted: (_) => {
                          _requestFocus(_passwordFocusnode),
                        },
                      ),
                      TextFormField(
                        onTap: () => {
                          _requestFocus(_passwordFocusnode),
                        },
                        decoration: InputDecoration(
                          labelText: localization.password,
                          hintText: localization.passwordHint,
                          labelStyle: TextStyle(
                            color: _passwordFocusnode.hasFocus
                                ? theme.focusColor
                                : theme.hintColor,
                          ),
                        ),
                        onSaved: (newValue) =>
                            user = user.copyWith(password: newValue),
                        validator: (value) {
                          validatorProvider.userPassword = value!;
                          switch (validatorProvider.passwordValidator(
                            value,
                          )) {
                            case isEmpty:
                              {
                                return localization.isEmptyError;
                              }
                            case isNotValid:
                              {
                                return localization.invalidPasswordError;
                              }

                            default:
                              {
                                //this if is isValid
                                return null;
                              }
                          }
                        },
                        focusNode: _passwordFocusnode,
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: true,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            showDialog(
                              context: context,
                              builder: (context) {
                                return const LoadIndicator();
                              },
                            );
                            _formKey.currentState!.save();
                            userProvider
                                .logIn(user.email!, user.password!)
                                .timeout(const Duration(seconds: 5))
                                .then(
                                  (_) => Navigator.of(context).pop(),
                                );
                          }
                        },
                        child: Text(
                          localization.logIn,
                          style: theme.textTheme.labelMedium,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: SizeConfig.foot1,
                child: Padding(
                  padding: const EdgeInsets.only(top: defaultPadding * 2),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            localization.areYouNew,
                            style: theme.textTheme.bodyMedium,
                          ),
                          TextButton(
                            onPressed: () => {
                              Navigator.of(context).pushNamed(
                                SignUpScreen.routeName,
                              ),
                            },
                            child: Text(
                              localization.signUp,
                              style: theme.textTheme.bodyMedium!.copyWith(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const MadeByMrGasha(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
