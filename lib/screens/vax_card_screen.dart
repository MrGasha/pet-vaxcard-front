import 'package:flutter/material.dart';
import 'package:pdf/pdf.dart' as pw;
import 'package:pet_vax_app/pdf/pdf_API.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../pdf/vax_card/vax_card_pdf.dart';
//providers
import '../providers/validator_provider.dart';
import '../providers/vaccination_provider.dart';
import '../providers/pet_provider.dart';
import '../providers/user_provider.dart';
//models
import '../models/pet_model.dart';
import '../models/vax_model.dart';
//constants
import '../size_config.dart';
import '../constans.dart';
//widgets
import '../widgets/load_indicator.dart';
import '../widgets/vax_list.dart';
import '../widgets/tab_widget/tabs/pet_tab/pet_view.dart';
import '../widgets/top_padding.dart';
import '../widgets/vax_modal.dart';
import '../widgets/made_by_mrgasha_text_btn.dart';

class VaxCardScreen extends StatefulWidget {
  static const routName = 'VaxScreen/';
  const VaxCardScreen({
    super.key,
  });
  @override
  State<VaxCardScreen> createState() => _VaxCardScreenState();
}

class _VaxCardScreenState extends State<VaxCardScreen> {
  bool _isInit = false;

  @override
  void didChangeDependencies() async {
    if (!_isInit) {
      final VaccinationProvider vaccinationProvider =
          Provider.of<VaccinationProvider>(
        context,
        listen: false,
      );
      final selectedPet =
          Provider.of<PetProvider>(context, listen: false).selectedPet;
      final UserProvider userProvider =
          Provider.of<UserProvider>(context, listen: false);
      if (await userProvider.isAuth) {
        final String? accessToken = await userProvider.accessToken;
        vaccinationProvider.getVaxByPet(selectedPet.id!, accessToken!);
      }
    }
    _isInit = true;

    super.didChangeDependencies();
  }

  void showVaxModal(BuildContext context, int petId) {
    showModalBottomSheet(
      context: context,
      builder: (context) => VaxModal(petId: petId),
      isScrollControlled: true,
      backgroundColor: Theme.of(context).backgroundColor,
    ).then(
      (value) =>
          Provider.of<ValidatorProvider>(context, listen: false).wipeData(),
    );
  }

  @override
  Widget build(BuildContext context) {
    final VaccinationProvider vaccinationProvider =
        Provider.of<VaccinationProvider>(context);
    final ThemeData theme = Theme.of(context);
    final PetProvider petProvider =
        Provider.of<PetProvider>(context, listen: false);
    final AppLocalizations localization = AppLocalizations.of(context)!;
    final Pet pet = petProvider.selectedPet;
    SizeConfig().init(context);

    return Container(
        alignment: Alignment.center,
        height: SizeConfig.screenHeight,
        width: SizeConfig.screenWidth,
        child: Scaffold(
          backgroundColor: theme.hintColor,
          body: !_isInit
              ? const Center(child: LoadIndicator())
              : Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    const TopPadding(),
                    SizedBox(
                      height: SizeConfig.tittleVaxScreen,
                      width: SizeConfig.screenWidth,
                      child: Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.topLeft,
                            child: IconButton(
                                onPressed: () => Navigator.of(context).pop(),
                                icon: const Icon(Icons.arrow_back),
                                color: theme.backgroundColor),
                          ),
                          SizedBox(
                            height: SizeConfig.tittleVaxScreen!,
                            width: SizeConfig.screenWidth!,
                            child: PetDetail(
                              pet: pet,
                              theme: theme,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: SizeConfig.screenWidth! * 0.9,
                      height: SizeConfig.bodyVaxScreen,
                      child: vaccinationProvider.vaccines.isEmpty
                          ? Center(
                              child: Text(
                                localization.noVaxText,
                                style: theme.textTheme.headlineSmall!
                                    .copyWith(color: Colors.black),
                                textAlign: TextAlign.center,
                              ),
                            )
                          : VaxList(
                              onDelete: vaccinationProvider.deleteVaccine,
                              vaccines: vaccinationProvider.vaccines,
                            ),
                    ),
                    SizedBox(
                      height: SizeConfig.footVaxScreen,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          SizedBox(
                            width: SizeConfig.blockSizeHorizontal! * 90,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                ElevatedButton(
                                  style: ButtonStyle(
                                    minimumSize: MaterialStateProperty.all(
                                      Size(
                                        SizeConfig.blockSizeHorizontal! * 30,
                                        SizeConfig.blockSizeVertical! * 5,
                                      ),
                                    ),
                                    backgroundColor: MaterialStateProperty.all(
                                        theme.backgroundColor),
                                    textStyle: MaterialStateProperty.all(
                                      theme.textTheme.titleMedium!.copyWith(
                                        color: theme.primaryColor,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  onPressed: () {
                                    showVaxModal(context, pet.id!);
                                  },
                                  child: Text(
                                    localization.addVax,
                                    style:
                                        theme.textTheme.titleMedium!.copyWith(
                                      color: theme.primaryColor,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                ElevatedButton(
                                  style: ButtonStyle(
                                    minimumSize: MaterialStateProperty.all(
                                      Size(
                                        SizeConfig.blockSizeHorizontal! * 30,
                                        SizeConfig.blockSizeVertical! * 5,
                                      ),
                                    ),
                                    backgroundColor: MaterialStateProperty.all(
                                        theme.backgroundColor),
                                    textStyle: MaterialStateProperty.all(
                                      theme.textTheme.titleMedium!.copyWith(
                                        color: theme.primaryColor,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  onPressed: () async {
                                    final petPdf = await generateCard(
                                      pet: pet,
                                      vaccines: vaccinationProvider.vaccines,
                                      buildContext: context,
                                    );
                                    PdfApi.openFile(petPdf);
                                  },
                                  child: Text(
                                    localization.savePdf,
                                    style:
                                        theme.textTheme.titleMedium!.copyWith(
                                      color: theme.primaryColor,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          MadeByMrGasha(
                            color: theme.backgroundColor,
                          )
                        ],
                      ),
                    )
                  ],
                ),
        ));
  }
}
