import 'dart:io';
import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import '../pdf_API.dart';
import '../pdf_theme.dart';
//models
import '../../models/pet_model.dart';
import '../../models/vax_model.dart';
//pdfWidgets
import './pet_detail_pdf.dart';
import './vax_detail_list_pdf.dart';

Future<File> generateCard({
  required Pet pet,
  required List<Vaccine> vaccines,
  required BuildContext buildContext,
}) async {
  final doc = pw.Document(title: 'Vax Card', author: 'MrGasha');

  Future<pw.MemoryImage> getVaxTag(Vaccine vaccine) async {
    if (vaccine.tagUrl != null) {
      var response = await http.get(Uri.parse(vaccine.tagUrl!));
      var data = response.bodyBytes;
      pw.MemoryImage vaxTag = pw.MemoryImage(data);
      return vaxTag;
    } else {
      pw.MemoryImage vaxTag = pw.MemoryImage(
        (await rootBundle.load('assets/images/defaultPic.jpg'))
            .buffer
            .asUint8List(),
      );
      return vaxTag;
    }
  }

  Future<pw.MemoryImage> getPetImage(String? picUrl) async {
    if (pet.picUrl != null) {
      var response = await http.get(Uri.parse(pet.picUrl!));
      var data = response.bodyBytes;
      pw.MemoryImage petImage = pw.MemoryImage(data);
      return petImage;
    } else {
      pw.MemoryImage petImage = pw.MemoryImage(
        (await rootBundle.load('assets/images/defaultPic.jpg'))
            .buffer
            .asUint8List(),
      );
      return petImage;
    }
  }

  pw.MemoryImage petImage = await getPetImage(pet.picUrl);

  const PdfPageFormat format = PdfPageFormat.letter;
  final pageTheme = await myPageTheme(format);
  Future<List<List<Vaccine>>> getFormatedVaccines(
      List<Vaccine> vaccines) async {
    List<Vaccine> vaccineList = [];
    const int listLength = 2;
    int acum = 1;
    List<List<Vaccine>> result = [];
    for (var vax in vaccines) {
      if (acum > listLength) {
        acum = 1;
        result.add(vaccineList);
        vaccineList = [];
      }
      vaccineList.add(vax.copyWith(
        tagPdf: await getVaxTag(vax),
      ));
      acum += 1;
    }
    if (vaccineList.isNotEmpty) {
      result.add(vaccineList);
    }
    return result;
  }

  List<List<Vaccine>> formatedVaccines = await getFormatedVaccines(vaccines);

  doc.addPage(
    pw.MultiPage(
        pageTheme: pageTheme,
        crossAxisAlignment: pw.CrossAxisAlignment.center,
        build: (pw.Context context) {
          return [
            pw.GridView(
              crossAxisCount: 3,
              childAspectRatio: 1.1,
              children: <pw.Widget>[
                PetDetailPdf(
                    pet: pet, petImage: petImage, buildContext: buildContext),
                ...formatedVaccines.map(
                  (vaccineList) => VaxDetailListPdf(
                    vaccines: vaccineList,
                    buildContext: buildContext,
                  ),
                ),
              ],
            ),
          ];
        }),
  );
  return PdfApi.saveDoc(name: '${pet.name}_vaccinations.pdf', pdf: doc);
}
