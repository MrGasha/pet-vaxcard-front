import 'dart:async';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
//constants
import '../../constans.dart';
//models
import '../../models/vax_model.dart';

class VaxDetailPdf extends pw.StatelessWidget {
  final Vaccine vaccine;
  final BuildContext buildContext;
  VaxDetailPdf({
    required this.vaccine,
    required this.buildContext,
  });

  @override
  pw.Widget build(pw.Context context) {
    final AppLocalizations localization = AppLocalizations.of(buildContext)!;
    return pw.Container(
      decoration: pw.BoxDecoration(
        borderRadius: pw.BorderRadius.circular(5),
        color: pdfPrimary,
      ),
      margin: const pw.EdgeInsets.only(top: 8),
      padding: const pw.EdgeInsets.all(5),
      width: 240,
      height: 135,
      child: pw.Column(
        mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
        children: <pw.Widget>[
          pw.Text(vaccine.name!,
              style: pw.Theme.of(context)
                  .header3
                  .copyWith(color: pdfTextPrimaryColor)),
          pw.Row(
            mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
            children: <pw.Widget>[
              pw.Container(
                width: 115,
                height: 40,
                decoration: pw.BoxDecoration(
                  borderRadius: pw.BorderRadius.circular(15),
                  image: pw.DecorationImage(
                    image: vaccine.tagPdf!,
                    fit: pw.BoxFit.cover,
                  ),
                ),
              ),
              pw.Column(
                mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
                children: <pw.Widget>[
                  pw.Text(
                    localization.getApplicationDate(
                      DateFormat(
                        'dd/MM/yy',
                      ).format(
                        vaccine.applicationDate!,
                      ),
                    ),
                    style: pw.Theme.of(context)
                        .defaultTextStyle
                        .copyWith(color: pdfTextPrimaryColor, fontSize: 12),
                  ),
                  pw.Text(
                    vaccine.boosterDate != null
                        ? localization.boosterDate(
                            DateFormat(
                              'dd/MM/yy',
                            ).format(
                              vaccine.boosterDate!,
                            ),
                          )
                        : localization.oneTime,
                    style: pw.Theme.of(context)
                        .defaultTextStyle
                        .copyWith(color: pdfTextPrimaryColor, fontSize: 12),
                  ),
                ],
              ),
            ],
          ),
          pw.Container(
            width: 295,
            height: 45,
            child: pw.Text(
                '${localization.description}: ${vaccine.description!}',
                style: pw.Theme.of(context)
                    .defaultTextStyle
                    .copyWith(color: pdfTextPrimaryColor, fontSize: 10),
                textAlign: pw.TextAlign.justify),
          )
        ],
      ),
    );
  }
}
