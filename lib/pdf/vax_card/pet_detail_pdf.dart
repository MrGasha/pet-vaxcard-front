import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
//constants
import 'package:pet_vax_app/constans.dart';
//models
import '../../models/pet_model.dart';

class PetDetailPdf extends pw.StatelessWidget {
  final Pet pet;
  final pw.MemoryImage petImage;
  final BuildContext buildContext;
  PetDetailPdf({
    required this.pet,
    required this.petImage,
    required this.buildContext,
  });

  @override
  pw.Widget build(pw.Context context) {
    final AppLocalizations localization = AppLocalizations.of(buildContext)!;
    return pw.Column(
      mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
      children: <pw.Widget>[
        pw.Container(
          width: 60,
          height: 60,
          alignment: pw.Alignment.topCenter,
          decoration: pw.BoxDecoration(
            shape: pw.BoxShape.circle,
            border: pw.Border.all(
              width: 2,
              color: pdfPrimary,
            ),
            image: pw.DecorationImage(
              image: petImage,
              fit: pw.BoxFit.cover,
            ),
          ),
        ),
        pw.Row(
          mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
          children: <pw.Widget>[
            pw.Column(
              children: <pw.Widget>[
                pw.Text(
                  localization.name,
                  style: pw.Theme.of(context).header4,
                ),
                pw.Container(
                  width: 80,
                  height: 16,
                  alignment: pw.Alignment.center,
                  color: pdfPrimary,
                  child: pw.Text(
                    pet.name!,
                    style: pw.Theme.of(context)
                        .header5
                        .copyWith(color: pdfTextPrimaryColor),
                  ),
                )
              ],
            ),
            pw.Column(
              children: <pw.Widget>[
                pw.Text(
                  localization.birthDate,
                  style: pw.Theme.of(context).header4,
                ),
                pw.Container(
                  width: 80,
                  height: 16,
                  alignment: pw.Alignment.center,
                  color: pdfPrimary,
                  child: pw.Text(
                    DateFormat('dd/MM/yy').format(pet.birthdate!),
                    style: pw.Theme.of(context)
                        .header5
                        .copyWith(color: pdfTextPrimaryColor),
                  ),
                )
              ],
            ),
          ],
        ),
        pw.Row(
          mainAxisAlignment: pw.MainAxisAlignment.spaceAround,
          children: <pw.Widget>[
            pw.Column(
              children: <pw.Widget>[
                pw.Text(
                  localization.species,
                  style: pw.Theme.of(context).header4,
                ),
                pw.Container(
                  width: 80,
                  height: 16,
                  alignment: pw.Alignment.center,
                  color: pdfPrimary,
                  child: pw.Text(
                    pet.species!,
                    style: pw.Theme.of(context)
                        .header5
                        .copyWith(color: pdfTextPrimaryColor),
                  ),
                )
              ],
            ),
            pw.Column(
              children: <pw.Widget>[
                pw.Text(
                  localization.gender,
                  style: pw.Theme.of(context).header4,
                ),
                pw.Container(
                  width: 80,
                  height: 16,
                  alignment: pw.Alignment.center,
                  color: pdfPrimary,
                  child: pw.Text(
                    pet.gender!,
                    style: pw.Theme.of(context)
                        .header5
                        .copyWith(color: pdfTextPrimaryColor),
                  ),
                )
              ],
            ),
          ],
        ),
        pw.Column(
          children: <pw.Widget>[
            pw.Text(
              localization.description,
              style: pw.Theme.of(context).header4,
            ),
            pw.Container(
              width: 250,
              height: 120,
              padding: const pw.EdgeInsets.all(5),
              color: pdfPrimary,
              child: pw.Text(pet.description!,
                  style: pw.Theme.of(context)
                      .header5
                      .copyWith(color: pdfTextPrimaryColor),
                  textAlign: pw.TextAlign.center),
            )
          ],
        ),
      ],
    );
  }
}
