import 'package:pdf/pdf.dart';
import 'package:flutter/widgets.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:pet_vax_app/pdf/vax_card/vax_detail_pdf.dart';
//models
import '../../models/vax_model.dart';
//pdf widgets
import './pet_detail_pdf.dart';

class VaxDetailListPdf extends pw.StatelessWidget {
  final List<Vaccine> vaccines;
  final BuildContext buildContext;
  VaxDetailListPdf({
    required this.vaccines,
    required this.buildContext,
  });

  @override
  pw.Widget build(pw.Context context) {
    return pw.ListView(
      children: vaccines
          .map(
            (vaccine) =>
                VaxDetailPdf(vaccine: vaccine, buildContext: buildContext),
          )
          .toList(),
    );
  }
}
