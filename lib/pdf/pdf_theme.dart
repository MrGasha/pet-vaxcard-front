import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:pdf/pdf.dart';

Future<pw.PageTheme> myPageTheme(PdfPageFormat format) async {
  format = format.copyWith(
      marginBottom: 5, marginLeft: 5, marginRight: 5, marginTop: 5);
  return pw.PageTheme(
    pageFormat: format,
    orientation: pw.PageOrientation.landscape,
    theme: pw.ThemeData.withFont(
      base: await PdfGoogleFonts.urbanistRegular(),
      bold: await PdfGoogleFonts.urbanistBold(),
      icons: await PdfGoogleFonts.materialIcons(),
    ),
  );
}
