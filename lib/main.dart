import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:pet_vax_app/screens/vax_card_screen.dart';
import 'package:provider/provider.dart';
//providers
import './providers/validator_provider.dart';
import './providers/user_provider.dart';
import './providers/pet_provider.dart';
import './providers/vaccination_provider.dart';
//localization
import './l10n/l10n.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
//theme
import './theme.dart';
//widgets
import './widgets/load_indicator.dart';
//screens
import './screens/login_screen.dart';
import './screens/signup_screen.dart';
import './screens/home_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => ValidatorProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => UserProvider(),
        ),
        ChangeNotifierProxyProvider<UserProvider, PetProvider>(
          create: (_) => PetProvider(),
          update: (
            _,
            authUser,
            petProvider,
          ) =>
              petProvider!..update(authUser),
        ),
        ChangeNotifierProvider(
          create: (_) => VaccinationProvider(),
        ),
      ],
      child: Consumer<UserProvider>(
        builder: (context, user, _) => MaterialApp(
          title: 'MyPet Card',
          debugShowCheckedModeBanner: false,
          theme: lightTheme,
          home: FutureBuilder(
            future: user.isAuth,
            builder: (context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData) {
                return snapshot.data!
                    ? const HomeScreen()
                    : const LoginScreen();
              } else {
                return const LoadIndicator();
              }
            },
          ),
          supportedLocales: L10n.all,
          localizationsDelegates: const [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          routes: {
            HomeScreen.routName: (ctx) => const HomeScreen(),
            LoginScreen.routeName: (ctx) => const LoginScreen(),
            SignUpScreen.routeName: (ctx) => const SignUpScreen(),
            VaxCardScreen.routName: (ctx) => const VaxCardScreen(),
          },
        ),
      ),
    );
  }
}
