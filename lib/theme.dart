import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
//const
import './constans.dart';

final ThemeData lightTheme = ThemeData(
  brightness: Brightness.light,
  backgroundColor: bgColorLightTheme, //deprecated
  scaffoldBackgroundColor: bgColorLightTheme,
  dialogBackgroundColor: primaryColor,
  cardColor: primaryColor,
  cardTheme: cardThemeLight,
  canvasColor: bgColorLightTheme,
  primaryColor: primaryColor,
  focusColor: primaryColor,
  splashColor: primaryColor,
  textTheme: GoogleFonts.urbanistTextTheme().copyWith(
    headlineSmall: const TextStyle(
      color: primaryColor,
      fontWeight: FontWeight.w300,
    ),
    headlineLarge: const TextStyle(
      color: primaryColor,
      fontWeight: FontWeight.w400,
    ),
    bodyMedium:
        const TextStyle(color: primaryColor, fontWeight: FontWeight.normal),
    labelLarge: const TextStyle(
      color: bgColorLightTheme,
      fontWeight: FontWeight.bold,
    ),
    labelMedium: const TextStyle(
      color: bgColorLightTheme,
      fontWeight: FontWeight.bold,
    ),
  ),
  inputDecorationTheme: textFieldDecorationLight,
  hintColor: secondaryColorLightTheme,
  textSelectionTheme: const TextSelectionThemeData(
    selectionHandleColor: primaryColor,
    cursorColor: primaryColor,
  ),
  textButtonTheme: textButtonThemeLight,
  elevatedButtonTheme: elevatedButtonThemeLight,
  iconTheme: iconThemeLight,
  checkboxTheme: checkBoxThemeLight,
  progressIndicatorTheme: progressIndicatorThemeData,
  bottomSheetTheme: bottomSheetThemeDataLight,
  sliderTheme: sliderThemeData,
);

final TextButtonThemeData textButtonThemeLight = TextButtonThemeData(
  style: ButtonStyle(
    overlayColor: MaterialStateProperty.all(Colors.transparent),
  ),
);

final ElevatedButtonThemeData elevatedButtonThemeLight =
    ElevatedButtonThemeData(
  style: ButtonStyle(
    foregroundColor: MaterialStateProperty.all(bgColorLightTheme),
    backgroundColor: MaterialStateProperty.all(primaryColor),
    alignment: Alignment.center,
    padding: MaterialStateProperty.all(
      const EdgeInsets.symmetric(
        horizontal: defaultElevatedButtonPadding,
      ),
    ),
    minimumSize: MaterialStateProperty.all(const Size(100, 40)),
    shape: MaterialStateProperty.all(
      const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(
            defaultBorderRadius,
          ),
        ),
      ),
    ),
  ),
);

final CheckboxThemeData checkBoxThemeLight = CheckboxThemeData(
  checkColor: MaterialStateProperty.all(
    bgColorLightTheme,
  ),
  splashRadius: defaultSpashRadiusCheckBox,
  side: const BorderSide(
    color: primaryColor,
    width: checkBoxDefaultwidth,
  ),
  fillColor: MaterialStateProperty.all(
    primaryColor,
  ),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(
        defaultBorderRadius,
      ),
    ),
  ),
);

const IconThemeData iconThemeLight = IconThemeData(color: primaryColor);

const InputDecorationTheme textFieldDecorationLight = InputDecorationTheme(
  errorStyle: TextStyle(
    fontWeight: FontWeight.bold,
  ),
  focusColor: primaryColor,
  focusedBorder: UnderlineInputBorder(
    borderSide: BorderSide(
      color: primaryColor,
    ),
  ),
  enabledBorder: UnderlineInputBorder(
    borderSide: BorderSide(
      color: secondaryColorLightTheme,
    ),
  ),
);

const ProgressIndicatorThemeData progressIndicatorThemeData =
    ProgressIndicatorThemeData(
  circularTrackColor: bgColorLightTheme,
  color: primaryColor,
);

const BottomSheetThemeData bottomSheetThemeDataLight = BottomSheetThemeData(
  backgroundColor: Colors.transparent,
  shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(modalBorderRadius),
    ),
  ),
);

const CardTheme cardThemeLight = CardTheme(
  color: primaryColor,
  margin: EdgeInsets.zero,
);

final SliderThemeData sliderThemeData = SliderThemeData(
  activeTickMarkColor: auxColorLightTheme,
  thumbColor: auxColorLightTheme,
  inactiveTrackColor: primaryColor,
  activeTrackColor: primaryColor,
  overlayColor: auxColorLightTheme.withOpacity(0.4),
);
