import 'dart:io';
import 'package:flutter/material.dart';
//constants
import '../constans.dart';

class VaxTagPic extends StatelessWidget {
  final File? pic;
  final String? picURL;
  const VaxTagPic({
    this.pic,
    this.picURL,
    super.key,
  });

  Image get getImage {
    if (pic != null) {
      return Image.file(
        pic!,
        fit: BoxFit.cover,
      );
    }
    if (picURL != null) {
      return Image.network(
        picURL!,
        fit: BoxFit.cover,
      );
    }
    return Image.asset('assets/images/defaultTagPic.jpg', fit: BoxFit.cover);
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      return Container(
        decoration: const BoxDecoration(
          borderRadius:
              BorderRadius.all(Radius.circular(defaultBorderRadius * 3)),
        ),
        clipBehavior: Clip.hardEdge,
        child: getImage,
      );
    });
  }
}
