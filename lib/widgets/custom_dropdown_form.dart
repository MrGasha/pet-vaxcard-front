import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
//provider
import '../providers/validator_provider.dart';
//constants
import '../constans.dart';
import '../errors.dart';

class CustomDropDownForm extends StatelessWidget {
  final Function? dropdownCallBack;
  final String? dropdownValue;
  final List<String>? dropdownItems;
  final String? dropdownLabel;
  final String? hint;
  final Color? color;
  final Color? dropdownColor;
  final Color? labelColor;

  const CustomDropDownForm({
    this.dropdownCallBack,
    this.dropdownValue,
    this.dropdownItems,
    this.dropdownLabel,
    this.hint,
    this.color,
    this.dropdownColor,
    this.labelColor,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final ValidatorProvider validatorProvider =
        Provider.of<ValidatorProvider>(context);
    final AppLocalizations localizations = AppLocalizations.of(context)!;

    return LayoutBuilder(
      builder: (
        BuildContext context,
        BoxConstraints constraints,
      ) {
        return DropdownButtonFormField(
          itemHeight: 50,
          borderRadius: BorderRadius.circular(defaultBorderRadius),
          alignment: Alignment.center,
          isExpanded: true,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          decoration: InputDecoration(
              constraints: constraints,
              filled: true,
              fillColor: color ?? theme.primaryColor,
              hintStyle: theme.textTheme.labelMedium!.copyWith(
                  color: theme.backgroundColor,
                  fontWeight: FontWeight.bold,
                  overflow: TextOverflow.visible),
              floatingLabelStyle:
                  const TextStyle(overflow: TextOverflow.visible),
              suffixStyle: const TextStyle(overflow: TextOverflow.visible),
              errorStyle: const TextStyle(
                overflow: TextOverflow.visible,
                fontWeight: FontWeight.bold,
              ),
              focusedErrorBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              border: InputBorder.none),
          items: dropdownItems!.map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem(
              value: value,
              alignment: Alignment.center,
              child: SizedBox(
                child: Text(
                  value,
                  overflow: TextOverflow.visible,
                ),
              ),
            );
          }).toList(),
          onChanged: (selectedValue) => {
            dropdownCallBack!(selectedValue),
          },
          dropdownColor: dropdownColor ?? theme.hintColor,
          style: theme.textTheme.labelMedium!.copyWith(
            overflow: TextOverflow.visible,
            color: labelColor ?? theme.backgroundColor,
          ),
          value: dropdownValue,
          validator: (selectedValue) {
            if (validatorProvider.dropDownValidator(
                    dropdownItems!.indexOf(selectedValue!)) ==
                isNotValid) {
              return localizations.noSelectionError;
            }
            return null;
          },
        );
      },
    );
  }
}
