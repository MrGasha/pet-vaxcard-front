import 'package:flutter/material.dart';
//constants
import '../constans.dart';
//widgets
import './avatar_btn.dart';

class AvatarButtonList extends StatelessWidget {
  final List<AvatarButton> avatarList;
  const AvatarButtonList({
    required this.avatarList,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListView.builder(
        itemCount: avatarList.length,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.only(right: defaultPadding),
            child: avatarList[index],
          );
        },
      ),
    );
  }
}
