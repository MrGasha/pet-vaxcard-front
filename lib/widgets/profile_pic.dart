import 'dart:io';
import 'package:flutter/material.dart';

class ProfilePic extends StatelessWidget {
  final File? pic;
  final String? picURL;
  final Color? color;
  const ProfilePic({
    this.pic,
    this.picURL,
    this.color,
    super.key,
  });

  Image get getImage {
    if (pic != null) {
      return Image.file(
        pic!,
        fit: BoxFit.cover,
      );
    }
    if (picURL != null) {
      return Image.network(
        picURL!,
        fit: BoxFit.cover,
      );
    }
    return Image.asset('assets/images/defaultPic.jpg', fit: BoxFit.cover);
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Container(
          alignment: Alignment.center,
          width: constraints.maxWidth,
          height: constraints.minHeight,
          child: Card(
            shape: const CircleBorder(
              side: BorderSide(style: BorderStyle.none),
            ),
            color: color ?? Theme.of(context).primaryColor,
            child: Padding(
              padding: const EdgeInsets.all(3.0),
              child: Container(
                height: constraints.maxHeight,
                width: constraints.maxWidth,
                decoration: const ShapeDecoration(
                  shape: CircleBorder(
                    side: BorderSide(style: BorderStyle.none),
                  ),
                ),
                clipBehavior: Clip.antiAlias,
                child: getImage,
              ),
            ),
          ),
        );
      },
    );
  }
}
