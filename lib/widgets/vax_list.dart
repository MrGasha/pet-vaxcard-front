import 'package:flutter/material.dart';
import 'package:pet_vax_app/widgets/vax_card.dart';
//constants
import '../constans.dart';
//models
import '../models/vax_model.dart';

class VaxList extends StatelessWidget {
  final List<Vaccine> vaccines;
  final Function onDelete;
  const VaxList({
    required this.vaccines,
    required this.onDelete,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: vaccines.length,
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.only(bottom: defaultPadding / 2),
          child: VaxCard(
            vaccine: vaccines[index],
            onDelete: onDelete,
          ),
        );
      },
    );
  }
}
