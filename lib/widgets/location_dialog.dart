import 'package:flutter/material.dart';
import 'package:pet_vax_app/models/custom_models.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
//constants
import '../constans.dart';
import '../size_config.dart';
//providers
import '../providers/validator_provider.dart';
//widgets
import 'search_dropdown.dart';

class LocationDialog extends StatefulWidget {
  const LocationDialog({super.key});

  @override
  State<LocationDialog> createState() => _LocationDialogState();
}

class _LocationDialogState extends State<LocationDialog> {
  final TextEditingController countryController = TextEditingController();
  final TextEditingController stateController = TextEditingController();
  final TextEditingController cityController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final AppLocalizations localization = AppLocalizations.of(context)!;

    final ValidatorProvider validatorProvider =
        Provider.of<ValidatorProvider>(context);
    final ThemeData theme = Theme.of(context);

    SizeConfig().init(context);
    return Dialog(
      insetPadding: EdgeInsets.zero,
      backgroundColor: theme.backgroundColor,
      child: SizedBox(
        width: SizeConfig.blockSizeHorizontal! * 80,
        height: SizeConfig.blockSizeVertical! * 60,
        child: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(color: theme.primaryColor),
                width: constraints.maxWidth,
                height: constraints.maxHeight * 0.25,
                alignment: Alignment.center,
                child: Text(
                  localization.location,
                  style: theme.textTheme.displayMedium!.copyWith(
                    color: theme.backgroundColor,
                  ),
                ),
              ),
              SizedBox(
                height: constraints.maxHeight * 0.6,
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      SearchDropdown(
                        label: localization.country,
                        dropdownEditingController: countryController,
                        onTap: validatorProvider.getStatesByCountry,
                        items: validatorProvider.countryList
                            .map(
                              (e) => SearchDropDownItem(
                                itemName: e.countryName,
                                itemId: e.countryId,
                              ),
                            )
                            .toList(),
                      ),
                      validatorProvider.stateList.isNotEmpty
                          ? SearchDropdown(
                              label: localization.state,
                              onTap: validatorProvider.getCitiesByState,
                              dropdownEditingController: stateController,
                              items: validatorProvider.stateList
                                  .map(
                                    (e) => SearchDropDownItem(
                                      itemName: e.stateName,
                                      itemId: e.stateId,
                                    ),
                                  )
                                  .toList(),
                            )
                          : const SizedBox(),
                      validatorProvider.cityList.isNotEmpty
                          ? SearchDropdown(
                              label: localization.city,
                              dropdownEditingController: cityController,
                              onTap: validatorProvider.setLocation,
                              items: validatorProvider.cityList
                                  .map(
                                    (e) => SearchDropDownItem(
                                      itemName: e.cityName,
                                      itemId: e.cityId,
                                    ),
                                  )
                                  .toList(),
                            )
                          : const SizedBox(),
                    ],
                  ),
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  localization.accept,
                  style: theme.textTheme.labelMedium,
                ),
              )
            ],
          );
        }),
      ),
    );
  }
}
