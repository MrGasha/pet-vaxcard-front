import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
//models
import '../models/vax_model.dart';
//provider
import '../providers/user_provider.dart';
//constants
import '../constans.dart';
import '../size_config.dart';
//widgets
import '../widgets/vax_tag_pic.dart';

class VaxCard extends StatelessWidget {
  final Vaccine vaccine;
  final Function onDelete;
  const VaxCard({
    required this.vaccine,
    required this.onDelete,
    super.key,
  });
  void showTag(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    SizeConfig().init(context);
    showDialog(
      context: context,
      builder: (context) => Container(
        alignment: Alignment.center,
        height: SizeConfig.blockSizeVertical! * 60,
        width: SizeConfig.blockSizeHorizontal! * 90,
        child: SizedBox(
          height: SizeConfig.blockSizeVertical! * 50,
          width: SizeConfig.blockSizeHorizontal! * 70,
          child: VaxTagPic(
            picURL: vaccine.tagUrl,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final AppLocalizations localization = AppLocalizations.of(context)!;
    final UserProvider userProvider =
        Provider.of<UserProvider>(context, listen: false);
    SizeConfig().init(context);
    Offset tapPosition = const Offset(0.0, 0.0);
    void storePosition(TapDownDetails details) {
      tapPosition = details.globalPosition;
    }

    final RenderBox overlay =
        Overlay.of(context).context.findRenderObject() as RenderBox;
    return Card(
      color: theme.backgroundColor,
      child: GestureDetector(
        onTapDown: storePosition,
        onLongPress: () {
          showMenu(
            context: context,
            position: RelativeRect.fromRect(
                tapPosition &
                    const Size(40, 40), // smaller rect, the touch area
                Offset.zero & overlay.size // Bigger rect, the entire screen
                ),
            items: <PopupMenuEntry>[
              PopupMenuItem<int>(
                onTap: () => {
                  Future.delayed(
                    const Duration(seconds: 0),
                    () => {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            actionsAlignment: MainAxisAlignment.center,
                            content: Text(
                              localization.deleteEntry,
                              style: theme.textTheme.titleMedium!.copyWith(
                                fontWeight: FontWeight.w300,
                              ),
                            ),
                            actions: <Widget>[
                              TextButton(
                                child: Text(
                                  localization.cancel,
                                  style: theme.textTheme.labelLarge!.copyWith(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                onPressed: () => Navigator.of(context).pop(),
                              ),
                              TextButton(
                                onPressed: () async {
                                  if (await userProvider.isAuth) {
                                    final String? accessToken =
                                        await userProvider.accessToken;
                                    onDelete(vaccine, accessToken);
                                    if (context.mounted) {
                                      Navigator.of(context).pop();
                                    }
                                  }
                                },
                                child: Text(
                                  localization.deleteVaccine,
                                  style: theme.textTheme.labelLarge!.copyWith(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          );
                        },
                      )
                    },
                  )
                },
                value: 0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      localization.deleteVaccine,
                    ),
                    Icon(Icons.delete, color: theme.backgroundColor),
                  ],
                ),
              ),
            ],
          );
        },
        child: ExpansionTile(
          expandedCrossAxisAlignment: CrossAxisAlignment.center,
          childrenPadding: const EdgeInsets.symmetric(vertical: defaultPadding),
          textColor: theme.focusColor,
          title: Center(
            child: Text(
              vaccine.name!,
            ),
          ),
          trailing: const Icon(Icons.vaccines_outlined),
          iconColor: theme.focusColor,
          subtitle: Wrap(
            alignment: WrapAlignment.spaceBetween,
            children: <Widget>[
              Text(
                localization.getApplicationDate(
                  DateFormat(
                    'dd/MM/yy',
                  ).format(
                    vaccine.applicationDate!,
                  ),
                ),
              ),
              Text(
                vaccine.boosterDate != null
                    ? localization.boosterDate(
                        DateFormat(
                          'dd/MM/yy',
                        ).format(
                          vaccine.boosterDate!,
                        ),
                      )
                    : localization.oneTime,
              ),
            ],
          ),
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                ElevatedButton(
                  onPressed: () {
                    showTag(context);
                  },
                  child: Text(localization.tagPic),
                ),
                vaccine.boosterDate != null
                    ? Text(
                        localization.getPeriodicity(
                          '${(vaccine.applicationDate!.difference(
                                vaccine.boosterDate!,
                              ).inDays / 30).ceil().abs()}',
                        ),
                      )
                    : const SizedBox(),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: defaultPadding / 2),
              child: Column(
                children: <Widget>[
                  Text(
                    localization.description,
                    style: theme.textTheme.titleMedium!
                        .copyWith(color: theme.primaryColor),
                  ),
                  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(defaultBorderRadius * 2),
                    ),
                    margin: const EdgeInsets.only(top: 8),
                    child: SizedBox(
                      width: SizeConfig.blockSizeHorizontal! * 80,
                      child: Padding(
                        padding: const EdgeInsets.all(defaultPadding),
                        child: Text(
                          vaccine.description!,
                          textAlign: TextAlign.center,
                          style: theme.textTheme.bodyLarge!
                              .copyWith(color: theme.backgroundColor),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
    ;
  }
}
