import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
//models
import '../models/custom_models.dart';
//widgets
import './custom_expansion_tile.dart';

class SearchDropdown extends StatefulWidget {
  final String? label;
  final String? hint;
  final Function? onTap;
  final TextEditingController dropdownEditingController;
  final List<SearchDropDownItem> items;
  const SearchDropdown({
    this.label,
    this.hint,
    this.onTap,
    required this.dropdownEditingController,
    required this.items,
    super.key,
  });

  @override
  State<SearchDropdown> createState() => _SearchDropdownState();
}

//Theming de esto
class _SearchDropdownState extends State<SearchDropdown> {
  final GlobalKey<CustomExpansionTileState> expansionTile =
      GlobalKey<CustomExpansionTileState>();
  final FocusNode _dropdownFocusNode = FocusNode();

  @override
  void dispose() {
    _dropdownFocusNode.dispose();
    super.dispose();
  }

  void _requestFocus() {
    FocusScope.of(context).requestFocus(_dropdownFocusNode);
  }

  void _onTileTap(item) {
    widget.dropdownEditingController.text = item.itemName;
    if (widget.onTap != null) {
      widget.onTap!(item.itemId);
    }
  }

  void _onTapTextField() {
    expansionTile.currentState?.expand();
    _requestFocus();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    List<SearchDropDownItem> filteredItems() {
      return widget.dropdownEditingController.text.isNotEmpty
          ? widget.items.where((entry) {
              return entry.itemName.toLowerCase().contains(
                  widget.dropdownEditingController.text.toLowerCase());
            }).toList()
          : widget.items;
    }

//todo: Delete textfield text when taped on, so it can show all items again
    return CustomExpansionTile(
      key: expansionTile,
      title: TextField(
        focusNode: _dropdownFocusNode,
        decoration: InputDecoration(
          labelText: widget.label,
          hintText: widget.hint,
          labelStyle: TextStyle(
            color: _dropdownFocusNode.hasFocus
                ? theme.focusColor
                : theme.hintColor,
          ),
        ),
        controller: widget.dropdownEditingController,
        onTap: () {
          _onTapTextField();
        },
        onChanged: (_) {
          setState(() {
            filteredItems(); //Se esta renderizando dos veces por llamada :(, sin esto no filtra
          });
        },
      ),
      children: filteredItems()
          .map(
            (e) => ListTile(
              title: Text(e.itemName),
              onTap: () {
                _onTileTap(e);
                expansionTile.currentState?.collapse();
              },
            ),
          )
          .toList(),
    );
  }
}
