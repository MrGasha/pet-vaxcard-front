import 'package:flutter/material.dart';
import 'package:pet_vax_app/size_config.dart';

class LoadIndicator extends StatelessWidget {
  const LoadIndicator({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      alignment: Alignment.center,
      width: SizeConfig.blockSizeHorizontal! * 25,
      height: SizeConfig.blockSizeHorizontal! * 25,
      child: const CircularProgressIndicator.adaptive(
        strokeWidth: 3,
      ),
    );
  }
}
