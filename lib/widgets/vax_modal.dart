import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
//models
import '../models/vax_model.dart';
//provider
import '../providers/validator_provider.dart';
import '../providers/vaccination_provider.dart';
import '../providers/user_provider.dart';
//constants
import '../size_config.dart';
import '../constans.dart';
import '../errors.dart';
//widgets
import './vax_tag_pic.dart';
import './camera_icon_btn.dart';

class VaxModal extends StatefulWidget {
  final int petId;
  const VaxModal({required this.petId, super.key});

  @override
  State<VaxModal> createState() => _VaxModalState();
}

class _VaxModalState extends State<VaxModal> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final FocusNode _vaxNameFocusNode = FocusNode();
  final FocusNode _descriptionFocusNode = FocusNode();
  bool oneTime = false;

  Vaccine vaccine = Vaccine(
    oneTime: false,
  );

  double sliderVal = 12;
  void _requestFocus(focusNode) {
    setState(
      () {
        FocusScope.of(context).requestFocus(focusNode);
      },
    );
  }

  void onCheckBoxChanged() {
    setState(() {
      oneTime = !oneTime;
      vaccine = vaccine.copyWith(oneTime: oneTime);
    });
  }

  void onSliderChange(
    newValue,
    DateTime selectedDate,
    bool isDateConfirmed,
  ) {
    setState(
      () {
        sliderVal = newValue;
        final DateTime? boosterDate = isDateConfirmed
            ? DateTime(selectedDate.year,
                selectedDate.month + sliderVal.round(), selectedDate.day)
            : null;
        vaccine = vaccine.copyWith(
          boosterDate: boosterDate,
        );
      },
    );
  }

  @override
  void dispose() {
    _vaxNameFocusNode.dispose();
    _descriptionFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final petId = widget.petId;

    final ThemeData theme = Theme.of(context);
    final ValidatorProvider validatorProvider =
        Provider.of<ValidatorProvider>(context);
    final UserProvider userProvider =
        Provider.of<UserProvider>(context, listen: false);
    final VaccinationProvider vaccinationProvider =
        Provider.of<VaccinationProvider>(context);

    final AppLocalizations localization = AppLocalizations.of(context)!;
    SizeConfig().init(context);

    return AnimatedPadding(
      duration: const Duration(milliseconds: 100),
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: BottomSheet(
        enableDrag: false,
        constraints: BoxConstraints.loose(
            Size(SizeConfig.screenHeight!, SizeConfig.blockSizeVertical! * 60)),
        onClosing: () {},
        builder: (context) => Padding(
          padding: const EdgeInsets.all(defaultPadding),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(
                  localization.registerVax,
                  style: theme.textTheme.headlineLarge!
                      .copyWith(fontWeight: FontWeight.w200),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          width: SizeConfig.blockSizeHorizontal! * 50,
                          child: TextFormField(
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            focusNode: _vaxNameFocusNode,
                            maxLength: 60,
                            maxLines: 1,
                            decoration: InputDecoration(
                              counterStyle:
                                  const TextStyle(color: Colors.transparent),
                              floatingLabelAlignment:
                                  FloatingLabelAlignment.center,
                              filled: true,
                              fillColor: theme.backgroundColor,
                              labelText: localization.vaxName,
                              hintText: localization.vaxNameHint,
                              labelStyle: TextStyle(
                                color: _vaxNameFocusNode.hasFocus
                                    ? theme.focusColor
                                    : theme.hintColor,
                              ),
                            ),
                            cursorColor: auxColorLightTheme,
                            textAlign: TextAlign.center,
                            onSaved: (newName) =>
                                vaccine = vaccine.copyWith(name: newName),
                            validator: (value) {
                              switch (validatorProvider.namesValidator(
                                value!,
                              )) {
                                case isEmpty:
                                  {
                                    return localization.isEmptyError;
                                  }
                                default:
                                  {
                                    //this if is isValid
                                    return null;
                                  }
                              }
                            },
                            onTap: () {
                              _requestFocus(_vaxNameFocusNode);
                            },
                          ),
                        ),
                        Column(
                          children: <Widget>[
                            ElevatedButton(
                              style: ButtonStyle(
                                textStyle: MaterialStateProperty.all(
                                    theme.textTheme.labelMedium),
                              ),
                              onPressed: () {
                                validatorProvider
                                    .getCalendar(context)
                                    .then((_) {
                                  onSliderChange(
                                    sliderVal,
                                    validatorProvider.selectedDate,
                                    validatorProvider.isDateConfirmed,
                                  );
                                });
                              },
                              child: Text(
                                validatorProvider.isDateConfirmed
                                    ? DateFormat('dd/MM/yy')
                                        .format(validatorProvider.selectedDate)
                                    : localization.applicationDate,
                                textAlign: TextAlign.center,
                              ),
                            ),
                            validatorProvider.isDateValid
                                ? const SizedBox()
                                : Text(
                                    localization.noSelectionError,
                                    overflow: TextOverflow.visible,
                                    style: theme.textTheme.bodySmall!.copyWith(
                                      color: theme.errorColor,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                          ],
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            SizedBox(
                                width: SizeConfig.blockSizeHorizontal! * 45,
                                height: SizeConfig.blockSizeVertical! * 12,
                                child: VaxTagPic(
                                  pic: validatorProvider.selectedImage,
                                )),
                            CameraIconButton(
                              isSelected:
                                  validatorProvider.selectedImage != null,
                              onPressed: () {
                                showDialog(
                                  context: context,
                                  builder: (context) {
                                    return Dialog(
                                      child: Container(
                                        alignment: Alignment.center,
                                        width: SizeConfig.blockSizeHorizontal! *
                                            50,
                                        height:
                                            SizeConfig.blockSizeVertical! * 20,
                                        child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: <Widget>[
                                              TextButton(
                                                onPressed: () {
                                                  validatorProvider
                                                      .selectImage(null);

                                                  Navigator.of(context).pop();
                                                },
                                                child: Text(
                                                    localization.librarySelect,
                                                    style: theme
                                                        .textTheme.titleLarge!
                                                        .copyWith(
                                                            color: theme
                                                                .backgroundColor)),
                                              ),
                                              TextButton(
                                                onPressed: () {
                                                  validatorProvider.selectImage(
                                                      ImageSource.camera);

                                                  Navigator.of(context).pop();
                                                },
                                                child: Text(
                                                    localization.cameraSelect,
                                                    style: theme
                                                        .textTheme.titleLarge!
                                                        .copyWith(
                                                            color: theme
                                                                .backgroundColor)),
                                              ),
                                            ]),
                                      ),
                                    );
                                  },
                                );
                              },
                            )
                          ],
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.only(bottom: defaultPadding * 2),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text(localization.oneTime),
                                  Checkbox(
                                      value: oneTime,
                                      onChanged: (_) => {
                                            onCheckBoxChanged(),
                                          })
                                ],
                              ),
                              oneTime
                                  ? const SizedBox()
                                  : Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          localization.periodicity,
                                          style: theme.textTheme.bodyMedium,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            SizedBox(
                                              width: SizeConfig
                                                      .blockSizeHorizontal! *
                                                  35,
                                              child: Slider(
                                                value: sliderVal,
                                                divisions: 23,
                                                onChanged: (newValue) {
                                                  onSliderChange(
                                                    newValue,
                                                    validatorProvider
                                                        .selectedDate,
                                                    validatorProvider
                                                        .isDateConfirmed,
                                                  );
                                                },
                                                min: 1,
                                                max: 24,
                                              ),
                                            ),
                                            Column(
                                              children: [
                                                Text(
                                                  '${sliderVal.round()}',
                                                  style: theme
                                                      .textTheme.labelLarge!
                                                      .copyWith(
                                                          color: theme
                                                              .primaryColor),
                                                ),
                                                Text(
                                                  localization.months,
                                                  style: theme
                                                      .textTheme.labelLarge!
                                                      .copyWith(
                                                          color: theme
                                                              .primaryColor),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        Text(localization.boosterDate(
                                            vaccine.boosterDate != null
                                                ? DateFormat(
                                                    'dd/MM/yy',
                                                  ).format(
                                                    vaccine.boosterDate!,
                                                  )
                                                : '')),
                                      ],
                                    ),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: SizeConfig.blockSizeVertical! * 15,
                      width: SizeConfig.blockSizeHorizontal! * 90,
                      child: TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        focusNode: _descriptionFocusNode,
                        decoration: InputDecoration(
                          focusedBorder: const OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(defaultBorderRadius * 3),
                            ),
                            borderSide: BorderSide(
                              color: primaryColor,
                            ),
                          ),
                          enabledBorder: const OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(defaultBorderRadius * 3),
                            ),
                            borderSide: BorderSide(
                              color: primaryColor,
                            ),
                          ),
                          errorBorder: const OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(defaultBorderRadius * 3),
                            ),
                            borderSide: BorderSide(
                              color: primaryColor,
                            ),
                          ),
                          focusedErrorBorder: const OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(defaultBorderRadius * 3),
                            ),
                            borderSide: BorderSide(
                              color: primaryColor,
                            ),
                          ),
                          floatingLabelAlignment: FloatingLabelAlignment.center,
                          labelText: localization.description,
                          hintText: localization.descriptionHint,
                          labelStyle: TextStyle(
                            color: _descriptionFocusNode.hasFocus
                                ? theme.focusColor
                                : theme.hintColor,
                          ),
                        ),
                        cursorColor: auxColorLightTheme,
                        maxLines: 4,
                        maxLength: 150,
                        textAlign: TextAlign.center,
                        onSaved: (newDescription) => vaccine =
                            vaccine.copyWith(description: newDescription),
                        validator: (value) {
                          switch (validatorProvider.namesValidator(
                            value!,
                          )) {
                            case isEmpty:
                              {
                                return localization.isEmptyError;
                              }

                            default:
                              {
                                //this if is isValid
                                return null;
                              }
                          }
                        },
                        onTap: () {
                          _requestFocus(_descriptionFocusNode);
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: defaultPadding),
                      child: ElevatedButton(
                        style: ButtonStyle(
                          textStyle: MaterialStateProperty.all(
                              theme.textTheme.labelMedium),
                        ),
                        onPressed: () async {
                          if (validatorProvider.isVaxFormValid(_formKey)) {
                            _formKey.currentState!.save();
                            final Vaccine newVax = vaccine.copyWith(
                              tag: validatorProvider.selectedImage,
                              applicationDate: validatorProvider.selectedDate,
                              petId: petId,
                            );
                            if (await userProvider.isAuth) {
                              final String accessToken =
                                  await userProvider.accessToken ?? '';
                              vaccinationProvider
                                  .addVax(newVax, accessToken)
                                  .then(
                                    (value) => Navigator.of(context).pop(),
                                  );
                            }
                          }
                        },
                        child: Text(
                          localization.addVax,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
