import 'package:flutter/material.dart';

class AddButton extends StatelessWidget {
  final Function onTap;
  const AddButton({
    required this.onTap,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ButtonStyle(
        shape: MaterialStateProperty.all(
          const CircleBorder(),
        ),
        minimumSize: MaterialStateProperty.all(
          const Size(50, 50),
        ),
      ),
      onPressed: () {
        onTap();
      },
      child: const Icon(Icons.add, size: 40),
    );
  }
}
