import 'package:flutter/material.dart';
//constants
import '../constans.dart';
import '../size_config.dart';

class CameraIconButton extends StatelessWidget {
  final Function? onPressed;
  final bool? isSelected;
  final Color? color;
  const CameraIconButton({
    this.isSelected,
    this.onPressed,
    this.color,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    SizeConfig().init(context);
    return IconButton(
      splashRadius: defaultIconSplashRadius,
      enableFeedback: true,
      color: color ?? theme.primaryColor,
      splashColor: color ?? theme.primaryColor,
      style: ButtonStyle(
        minimumSize: MaterialStateProperty.all(
          Size(
            SizeConfig.blockSizeHorizontal! * 5,
            SizeConfig.blockSizeHorizontal! * 5,
          ),
        ),
      ),
      onPressed: () => onPressed!(),
      icon: Icon(
        isSelected! ? Icons.camera_alt_rounded : Icons.camera_alt_outlined,
      ),
    );
  }
}
