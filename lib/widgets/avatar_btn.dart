import 'dart:io';
import 'package:flutter/material.dart';
//constants
import '../size_config.dart';
//widgets
import './profile_pic.dart';

class AvatarButton extends StatelessWidget {
  final File? picFile;
  final String? picUrl;
  final String name;
  final String? lastName;
  final Function ontap;
  const AvatarButton({
    this.picFile,
    this.picUrl,
    required this.name,
    required this.ontap,
    this.lastName,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      return Container(
        constraints: constraints,
        alignment: Alignment.center,
        child: InkWell(
          onTap: () {
            ontap();
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: SizeConfig.blockSizeHorizontal! * 30,
                width: SizeConfig.blockSizeHorizontal! * 30,
                child: ProfilePic(
                  pic: picFile,
                  picURL: picUrl,
                ),
              ),
              Row(
                children: [
                  Text(name),
                  Text(lastName ?? ''),
                ],
              ),
            ],
          ),
        ),
      );
    });
  }
}
