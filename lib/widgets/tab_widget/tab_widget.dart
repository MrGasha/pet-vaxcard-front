import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:pet_vax_app/size_config.dart';
import '../../models/custom_models.dart';
//widgets
import './tab_button.dart';
//tabs
import 'tabs/alert_tab/alert_tab_widget.dart';
import 'tabs/pet_tab/pet_tab_widget.dart';

class TabWidget extends StatefulWidget {
  const TabWidget({super.key});

  @override
  State<TabWidget> createState() => _TabWidgetState();
}

class _TabWidgetState extends State<TabWidget> {
  final PageController _pageController = PageController(initialPage: 1);
  final List<TabItem> tabs = [
    TabItem(
      tab: const AlertTab(),
      tabId: 0,
    ),
    TabItem(
      tab: const PetTab(),
      tabId: 1,
      isSelected: true,
    ),
  ];

  int selectedPage = 1;

  void selectPage(int page) {
    setState(
      () {
        selectedPage = page;
        TabItem currentTab = tabs[page];
        if (!currentTab.isSelected) {
          //if taped button is not selected
          currentTab.isSelected = true; //select it
          for (TabItem tab in tabs) {
            //deselect the others
            if (tab != currentTab) {
              tab.isSelected = false;
            }
          }
          _pageController.animateToPage(
            page,
            duration: const Duration(milliseconds: 500),
            curve: Curves.fastLinearToSlowEaseIn,
          );
        }
      },
    );
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final AppLocalizations localization = AppLocalizations.of(context)!;
    final ThemeData theme = Theme.of(context);
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                TabButton(
                  //Alert Tab Button
                  onTap: () => selectPage(0),
                  label: localization.alerts,
                  isSelected: tabs[0].isSelected,
                ),
                TabButton(
                  //Alert Tab Button
                  onTap: () => selectPage(1),
                  label: localization.pets,
                  isSelected: tabs[1].isSelected,
                ),
              ],
            ),
            SizedBox(
              height: constraints.maxHeight * 0.8,
              width: constraints.maxWidth,
              child: NotificationListener<OverscrollIndicatorNotification>(
                onNotification: (OverscrollIndicatorNotification overscroll) {
                  if (overscroll.leading) {
                    overscroll.paintOffset = 1;
                  }
                  return false;
                },
                child: PageView(
                  controller: _pageController,
                  onPageChanged: (page) {
                    selectPage(page);
                  },
                  children: tabs.map((e) => e.tab).toList(),
                ),
              ),
            ),
          ],
        ),
      );
    });
  }
}
