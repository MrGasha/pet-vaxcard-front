import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:pet_vax_app/constans.dart';
//models
import '../../../../models/pet_model.dart';
//constants
import '../../../../size_config.dart';
import '../../../../theme.dart';

class ReminderCard extends StatelessWidget {
  final Reminder reminder;
  const ReminderCard({
    required this.reminder,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final AppLocalizations localization = AppLocalizations.of(context)!;
    final ThemeData theme = Theme.of(context);
    String getReminderMessage({
      required int reminderType,
      required String petName,
      required String vaccineName,
      required DateTime boosterDate,
    }) {
      final String fBoosterDate = DateFormat(
        'dd/MM/yy',
      ).format(
        boosterDate,
      );
      switch (reminderType) {
        case reminder24H:
          return localization.reminder24H(
            petName,
            vaccineName,
            fBoosterDate,
          );
        case reminder48H:
          return localization.reminder48H(
            petName,
            vaccineName,
            fBoosterDate,
          );
        case reminder1W:
          return localization.reminder1W(
            petName,
            vaccineName,
            fBoosterDate,
          );
        case reminder1M:
          return localization.reminder1M(
            petName,
            vaccineName,
            fBoosterDate,
          );
        default:
          return localization.reminder3M(
            petName,
            vaccineName,
            fBoosterDate,
          );
      }
    }

    SizeConfig().init(context);
    return Card(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(
            defaultBorderRadius * 4,
          ),
        ),
      ),
      margin: const EdgeInsets.symmetric(horizontal: defaultPadding * 2),
      child: Container(
          height: SizeConfig.blockSizeVertical! * 12,
          alignment: Alignment.center,
          margin: const EdgeInsets.all(defaultPadding / 2),
          child: Text(
            getReminderMessage(
              reminderType: reminder.reminderType!,
              petName: reminder.petName!,
              vaccineName: reminder.vaccine!.name!,
              boosterDate: reminder.vaccine!.boosterDate!,
            ),
            style: theme.textTheme.titleMedium!.copyWith(
              color: theme.backgroundColor,
            ),
            textAlign: TextAlign.center,
          )),
    );
  }
}
