import 'package:flutter/material.dart';
//widgets
import './reminder_card.dart';
//models
import '../../../../models/pet_model.dart';
//constants
import '../../../../constans.dart';

class ReminderList extends StatelessWidget {
  final List<Reminder> reminders;
  const ReminderList({
    required this.reminders,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: reminders.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.only(
            bottom: defaultPadding / 2,
          ),
          child: ReminderCard(
            reminder: reminders[index],
          ),
        );
      },
    );
  }
}
