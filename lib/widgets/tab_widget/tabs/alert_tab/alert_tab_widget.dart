import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
//providers
import '../../../../providers/pet_provider.dart';

//constants
import '../../../../constans.dart';
//widgets
import './reminder_list.dart';

class AlertTab extends StatelessWidget {
  const AlertTab({super.key});

  @override
  Widget build(BuildContext context) {
    final AppLocalizations localization = AppLocalizations.of(context)!;
    final ThemeData theme = Theme.of(context);
    return Consumer<PetProvider>(
      builder: (context, petProvider, child) => LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return Container(
            width: constraints.maxWidth,
            height: constraints.maxHeight,
            alignment: Alignment.topCenter,
            child: petProvider.reminders.isEmpty
                ? Padding(
                    padding: const EdgeInsets.only(top: defaultPadding * 4),
                    child: Text(
                      localization.noReminderText,
                      style: theme.textTheme.headlineSmall,
                      textAlign: TextAlign.center,
                    ),
                  )
                : SizedBox(
                    height: constraints.maxHeight,
                    width: constraints.maxWidth,
                    child: ReminderList(
                      reminders: petProvider.reminders,
                    ),
                  ),
          );
        },
      ),
    );
  }
}
