import 'package:flutter/material.dart';
import 'package:pet_vax_app/providers/validator_provider.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
//models
import '../../../../models/pet_model.dart';
//providers
import '../../../../providers/pet_provider.dart';
//widgets
import '../../../add_button.dart';
import './pet_modal.dart';
import '../../../avatar_btn_list.dart';
import '../../../avatar_btn.dart';

class PetTab extends StatelessWidget {
  const PetTab({super.key});
  //this may be on petProvider
  void showPetModal(BuildContext context,
      {required bool isForm, Pet? pet, petMeta}) {
    if (!isForm) {
      pet = pet?.copyWith(
        species: petMeta[0][pet.speciesId],
        gender: petMeta[1][pet.genderId],
      );
      final PetProvider petProvider =
          Provider.of<PetProvider>(context, listen: false);
      petProvider.selectedPet = pet!;
    }

    showModalBottomSheet(
      context: context,
      builder: (context) => PetModal(
        isForm: isForm,
        pet: pet,
        petMeta: petMeta,
      ),
      isScrollControlled: true,
    ).then(
      (value) => isForm
          ? Provider.of<ValidatorProvider>(context, listen: false).wipeData()
          : null,
    );
  }

  @override
  Widget build(BuildContext context) {
    final AppLocalizations localization = AppLocalizations.of(context)!;
    final ThemeData theme = Theme.of(context);

    final List<String> speciesList = <String>[
      localization.species,
      localization.cat,
      localization.dog,
      localization.other,
    ];
    final List<String> genderList = <String>[
      localization.gender,
      localization.male,
      localization.female,
      localization.other,
    ];
    //the data used to format pets using their genderId and speciesId
    final List<List<String>> petMeta = <List<String>>[
      speciesList,
      genderList,
    ];

    return Consumer<PetProvider>(
      builder: ((context, petProvider, child) => LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              return Container(
                width: constraints.maxWidth,
                height: constraints.maxHeight,
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    petProvider.pets.isEmpty
                        ? Text(
                            localization.registerPetsText,
                            style: theme.textTheme.headlineSmall,
                            textAlign: TextAlign.center,
                          )
                        : SizedBox(
                            height: constraints.maxHeight * 0.5,
                            width: constraints.maxWidth,
                            child: AvatarButtonList(
                              avatarList: petProvider.pets.map(
                                (pet) {
                                  return AvatarButton(
                                    name: pet.name!,
                                    ontap: () {
                                      showPetModal(
                                        context,
                                        isForm: false,
                                        pet: pet,
                                        petMeta: petMeta,
                                      );
                                    },
                                    picUrl: pet.picUrl,
                                  );
                                },
                              ).toList(),
                            ),
                          ),
                    AddButton(
                      onTap: () {
                        showPetModal(context, isForm: true, petMeta: petMeta);
                      },
                    ),
                  ],
                ),
              );
            },
          )),
    );
  }
}
