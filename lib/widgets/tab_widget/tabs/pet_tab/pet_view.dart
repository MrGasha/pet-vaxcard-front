import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
//provider
import '../../../../providers/pet_provider.dart';
//constants
import '../../../../size_config.dart';
import '../../../../constans.dart';
//models
import '../../../../models/pet_model.dart';
//widgets
import '../../../profile_pic.dart';
//screens
import '../../../../screens/vax_card_screen.dart';

class PetView extends StatelessWidget {
  final Pet pet;
  const PetView({
    required this.pet,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final AppLocalizations localization = AppLocalizations.of(context)!;
    final PetProvider petProvider =
        Provider.of<PetProvider>(context, listen: false);
    SizeConfig().init(context);
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Container(
          alignment: Alignment.center,
          width: constraints.maxWidth,
          height: constraints.maxHeight,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: constraints.maxHeight * 0.85,
                width: constraints.maxWidth,
                child: PetDetail(
                  pet: pet,
                  theme: theme,
                ),
              ),
              SizedBox(
                width: constraints.maxWidth,
                height: constraints.maxHeight * 0.15,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    ElevatedButton(
                      style: ButtonStyle(
                        minimumSize: MaterialStateProperty.all(
                          Size(
                            constraints.maxWidth * 0.3,
                            constraints.maxHeight * 0.08,
                          ),
                        ),
                        backgroundColor:
                            MaterialStateProperty.all(theme.backgroundColor),
                        textStyle: MaterialStateProperty.all(
                          theme.textTheme.titleMedium!.copyWith(
                            color: theme.primaryColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      onPressed: () {
                        Navigator.of(context).pushNamed(
                          VaxCardScreen.routName,
                        );
                      },
                      child: Text(
                        localization.vaccines,
                        style: theme.textTheme.titleMedium!.copyWith(
                          color: theme.primaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    ElevatedButton(
                      style: ButtonStyle(
                        minimumSize: MaterialStateProperty.all(
                          Size(
                            constraints.maxWidth * 0.3,
                            constraints.maxHeight * 0.08,
                          ),
                        ),
                        backgroundColor:
                            MaterialStateProperty.all(theme.backgroundColor),
                        textStyle: MaterialStateProperty.all(
                          theme.textTheme.titleMedium!.copyWith(
                            color: theme.primaryColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      onPressed: () {},
                      child: Text(
                        localization.edit,
                        style: theme.textTheme.titleMedium!.copyWith(
                          color: theme.primaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

class PetDetail extends StatelessWidget {
  const PetDetail({
    super.key,
    required this.pet,
    required this.theme,
  });

  final Pet pet;
  final ThemeData theme;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          SizedBox(
            width: constraints.maxWidth * 0.35,
            height: constraints.maxWidth * 0.35,
            child: ProfilePic(
              picURL: pet.picUrl,
              color: theme.backgroundColor,
            ),
          ),
          SizedBox(
            height: constraints.maxHeight * 0.55,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                  width: constraints.maxWidth * 0.9,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Card(
                        color: theme.backgroundColor,
                        child: Container(
                          width: constraints.maxWidth *
                              0.3, //this value should be on sizeconfig or constants
                          height: constraints.maxHeight * 0.08,
                          alignment: Alignment.center,
                          child: Text(
                            pet.name!,
                          ),
                        ),
                      ),
                      Card(
                        color: theme.backgroundColor,
                        child: Container(
                          width: constraints.maxWidth *
                              0.3, //this value should be on sizeconfig
                          height: constraints.maxHeight * 0.08,
                          alignment: Alignment.center,
                          child: Text(
                            DateFormat('dd/MM/yy').format(pet.birthdate!),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: SizeConfig.blockSizeHorizontal! * 90,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Card(
                        color: theme.backgroundColor,
                        child: Container(
                          width: constraints.maxWidth *
                              0.3, //this value should be on sizeconfig
                          height: constraints.maxHeight * 0.08,
                          alignment: Alignment.center,
                          child: Text(
                            pet.species!,
                          ),
                        ),
                      ),
                      Card(
                        color: theme.backgroundColor,
                        child: Container(
                          width: constraints.maxWidth *
                              0.3, //this value should be on sizeconfig
                          height: constraints.maxHeight * 0.08,
                          alignment: Alignment.center,
                          child: Text(
                            pet.gender!,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Card(
                  color: theme.backgroundColor,
                  child: Container(
                    height: constraints.maxHeight * 0.25,
                    width: constraints.maxWidth * 0.9,
                    padding: const EdgeInsets.all(defaultPadding),
                    child: Text(
                      pet.description!,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      );
    });
  }
}
