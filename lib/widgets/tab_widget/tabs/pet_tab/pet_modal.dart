import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
//constants
import '../../../../size_config.dart';
//models
import '../../../../models/pet_model.dart';
//widgets
import './pet_form.dart';
import './pet_view.dart';

class PetModal extends StatelessWidget {
  final bool isForm;
  final Pet? pet;
  final List<List<String>> petMeta;
  const PetModal({
    required this.isForm,
    required this.petMeta,
    this.pet,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final AppLocalizations localization = AppLocalizations.of(context)!;
    final ThemeData theme = Theme.of(context);
    Pet? pet = this.pet;
    //the data used to format pets using their genderId and speciesId
    SizeConfig().init(context);
    return AnimatedPadding(
      duration: const Duration(milliseconds: 100),
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).viewInsets.bottom,
      ),
      child: BottomSheet(
        backgroundColor: theme.hintColor,
        enableDrag: false,
        constraints: BoxConstraints.loose(
          Size(
            SizeConfig.screenWidth!,
            SizeConfig.blockSizeVertical! * 60,
          ),
        ),
        builder: (context) => isForm
            ? PetForm(
                petMeta: petMeta,
              )
            : PetView(
                pet: pet!,
              ),
        onClosing: () {},
      ),
    );
  }
}
