import 'package:flutter/material.dart';
import 'package:pet_vax_app/size_config.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
//providers
import '../../../../providers/pet_provider.dart';
import '../../../../providers/validator_provider.dart';
import '../../../../providers/user_provider.dart';
//models
import '../../../../models/pet_model.dart';
import '../../../../models/custom_models.dart';
//constants
import '../../../../errors.dart';

//widgets
import '../../../camera_icon_btn.dart';
import '../../../profile_pic.dart';
import '../../../custom_dropdown_form.dart';

class PetForm extends StatefulWidget {
  final List<List<String>> petMeta;
  const PetForm({
    required this.petMeta,
    super.key,
  });

  @override
  State<PetForm> createState() => _PetFormState();
}

class _PetFormState extends State<PetForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final FocusNode _nameFocusNode = FocusNode();
  final FocusNode _descriptionNode = FocusNode();
  Pet pet = Pet();

  @override
  void dispose() {
    _nameFocusNode.dispose();
    _descriptionNode.dispose();
    super.dispose();
  }

  void _requestFocus(focusNode) {
    setState(
      () {
        FocusScope.of(context).requestFocus(focusNode);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final ValidatorProvider validatorProvider =
        Provider.of<ValidatorProvider>(context);
    final PetProvider petProvider = Provider.of<PetProvider>(context);
    final UserProvider userProvider =
        Provider.of<UserProvider>(context, listen: false);
    final ThemeData theme = Theme.of(context);
    final AppLocalizations localization = AppLocalizations.of(context)!;
    final List<String> speciesList = widget.petMeta[0];

    String speciesDropdownValue = localization.species;
    final List<String> genderList = widget.petMeta[1];
    String genderDropdownValue = localization.gender;

    void speciesDropdownCallBack(String selectedValue) {
      setState(
        () {
          speciesDropdownValue = selectedValue;

          pet = pet.copyWith(speciesId: speciesList.indexOf(selectedValue));
        },
      );
    }

    void genderDropdownCallBack(String selectedValue) {
      setState(
        () {
          genderDropdownValue = selectedValue;

          pet = pet.copyWith(genderId: genderList.indexOf(selectedValue));
        },
      );
    }

    SizeConfig().init(context);
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Container(
          alignment: Alignment.center,
          width: constraints.maxWidth,
          height: constraints.maxHeight,
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  width: constraints.maxWidth,
                ),
                SizedBox(
                  width: constraints.maxWidth * 0.35,
                  height: constraints.maxWidth * 0.35,
                  child: ProfilePic(
                    pic: validatorProvider.selectedImage,
                    color: theme.backgroundColor,
                  ),
                ),
                SizedBox(
                  width: constraints.maxWidth,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      SizedBox(
                        width: constraints.maxWidth * 0.4,
                        child: TextFormField(
                          focusNode: _nameFocusNode,
                          decoration: InputDecoration(
                            floatingLabelAlignment:
                                FloatingLabelAlignment.center,
                            filled: true,
                            fillColor: theme.backgroundColor,
                            labelText: localization.name,
                            hintText: localization.petNameHint,
                            labelStyle: TextStyle(
                              color: _nameFocusNode.hasFocus
                                  ? theme.focusColor
                                  : theme.hintColor,
                            ),
                          ),
                          textAlign: TextAlign.center,
                          onSaved: (newValue) =>
                              pet = pet.copyWith(name: newValue),
                          validator: (value) {
                            switch (validatorProvider.namesValidator(
                              value!,
                            )) {
                              case isEmpty:
                                {
                                  return localization.isEmptyError;
                                }
                              case isNotValid:
                                {
                                  return localization.invalidFieldError;
                                }
                              default:
                                {
                                  //this if is isValid
                                  return null;
                                }
                            }
                          },
                          onTap: () {
                            _requestFocus(_nameFocusNode);
                          },
                        ),
                      ),
                      CameraIconButton(
                        isSelected: validatorProvider.selectedImage != null,
                        onPressed: () => validatorProvider.selectImage(null),
                        color: theme.backgroundColor,
                      ),
                      Column(
                        children: <Widget>[
                          ElevatedButton(
                            style: ButtonStyle(
                              minimumSize: MaterialStateProperty.all(
                                Size(
                                  constraints.maxWidth * 0.4,
                                  55,
                                ),
                              ),
                              backgroundColor: MaterialStateProperty.all(
                                  theme.backgroundColor),
                              textStyle: MaterialStateProperty.all(
                                theme.textTheme.titleMedium!.copyWith(
                                  color: theme.primaryColor,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            onPressed: () {
                              validatorProvider.getCalendar(context);
                            },
                            child: Text(
                              validatorProvider.isDateConfirmed
                                  ? DateFormat('dd/MM/yy')
                                      .format(validatorProvider.selectedDate)
                                  : localization.birthDate,
                              style: theme.textTheme.titleMedium!.copyWith(
                                color: theme.primaryColor,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          validatorProvider.isDateValid
                              ? const SizedBox()
                              : Text(
                                  localization.noSelectionError,
                                  overflow: TextOverflow.visible,
                                  style: theme.textTheme.bodySmall!.copyWith(
                                    color: theme.errorColor,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                        ],
                      )
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    SizedBox(
                      width: constraints.maxWidth * 0.4,
                      child: CustomDropDownForm(
                        color: theme.backgroundColor,
                        dropdownColor: theme.primaryColor,
                        labelColor: theme.hintColor,
                        dropdownItems: speciesList,
                        dropdownLabel: localization.species,
                        dropdownValue: speciesDropdownValue,
                        dropdownCallBack: (selectedValue) {
                          speciesDropdownCallBack(selectedValue);
                        },
                      ),
                    ),
                    SizedBox(
                      width: constraints.maxWidth * 0.1,
                    ),
                    SizedBox(
                      width: constraints.maxWidth * 0.4,
                      child: CustomDropDownForm(
                        color: theme.backgroundColor,
                        dropdownColor: theme.primaryColor,
                        labelColor: theme.hintColor,
                        dropdownItems: genderList,
                        dropdownLabel: localization.gender,
                        dropdownValue: genderDropdownValue,
                        dropdownCallBack: (selectedValue) {
                          genderDropdownCallBack(selectedValue);
                        },
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  width: constraints.maxWidth * 0.98,
                  child: TextFormField(
                    focusNode: _descriptionNode,
                    maxLines: 5,
                    maxLength: 230,
                    textCapitalization: TextCapitalization.sentences,
                    decoration: InputDecoration(
                      floatingLabelAlignment: FloatingLabelAlignment.center,
                      filled: true,
                      fillColor: theme.backgroundColor,
                      labelText: localization.description,
                      hintText: localization.descriptionHint,
                      labelStyle: TextStyle(
                        color: _nameFocusNode.hasFocus
                            ? theme.focusColor
                            : theme.hintColor,
                      ),
                    ),
                    textAlign: TextAlign.center,
                    onSaved: (newValue) =>
                        pet = pet.copyWith(description: newValue),
                    validator: (value) {
                      switch (validatorProvider.descriptionValidator(
                        value!,
                      )) {
                        case isEmpty:
                          {
                            return localization.isEmptyError;
                          }
                        default:
                          {
                            //this if is isValid
                            return null;
                          }
                      }
                    },
                    onTap: () {
                      _requestFocus(_descriptionNode);
                    },
                  ),
                ),
                ElevatedButton(
                  style: ButtonStyle(
                    minimumSize: MaterialStateProperty.all(
                      Size(
                        constraints.maxWidth * 0.4,
                        55,
                      ),
                    ),
                    backgroundColor:
                        MaterialStateProperty.all(theme.backgroundColor),
                    textStyle: MaterialStateProperty.all(
                      theme.textTheme.titleMedium!.copyWith(
                        color: theme.primaryColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  onPressed: () async {
                    if (validatorProvider.isPetFormValid(_formKey)) {
                      _formKey.currentState!.save();
                      final Pet newPet = pet.copyWith(
                        pic: validatorProvider.selectedImage,
                        birthdate: validatorProvider.selectedDate,
                      );
                      if (await userProvider.isAuth) {
                        //Delay register pet, so its called after update
                        //Delay a function call by a random amount
                        //does not seems like the correct way
                        //THIS IS A LAST RESOURCE!
                        //maybe await accestoken directly instead of using proxy

                        Future.delayed(
                          const Duration(milliseconds: 100),
                          () => petProvider.registerPet(newPet).then(
                                (value) => Navigator.of(context).pop(),
                              ),
                        );
                      }
                    }
                  },
                  child: Text(
                    localization.save,
                    style: theme.textTheme.titleMedium!.copyWith(
                      color: theme.primaryColor,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
