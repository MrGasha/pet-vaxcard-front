import 'package:flutter/material.dart';
//constants
import '../../constans.dart';

class TabButton extends StatelessWidget {
  final Function onTap;
  final String label;
  final bool isSelected;
  const TabButton({
    required this.onTap,
    required this.label,
    required this.isSelected,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return TextButton(
      onPressed: () => {onTap()},
      style: ButtonStyle(
        backgroundColor: isSelected
            ? MaterialStateProperty.all(theme.focusColor)
            : MaterialStateProperty.all(Colors.transparent),
        shape: MaterialStateProperty.all(
          const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(
                tabButtonBorderRadius,
              ),
            ),
          ),
        ),
      ),
      child: Text(
        label,
        style: theme.textTheme.titleMedium!.copyWith(
          fontWeight: FontWeight.bold,
          color: isSelected ? theme.backgroundColor : theme.primaryColor,
        ),
      ),
    );
  }
}
