import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class MadeByMrGasha extends StatelessWidget {
  final Color? color;
  const MadeByMrGasha({
    this.color,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomRight,
      child: TextButton(
        onPressed: () => {},
        child: Text(
          AppLocalizations.of(context)!.madeByMrGasha,
          style: Theme.of(context).textTheme.bodySmall!.copyWith(
                color: color ?? Theme.of(context).primaryColor,
                fontWeight: FontWeight.bold,
              ),
        ),
      ),
    );
  }
}
