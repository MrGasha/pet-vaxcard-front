import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:path/path.dart'; //for basename
import 'package:http/http.dart' as http;
import 'package:pet_vax_app/constans.dart';
//models
import '../models/vax_model.dart';

class VaccinationProvider with ChangeNotifier {
  static const String addVaxURL = '$serverDomain/add_vax/';
  static const String getVaxByPetURL = '$serverDomain/get_vax_by_pet/';
  static const String deleteVaccineById = '$serverDomain/delete_vax/';
  Map<String, String> headers(String accessToken) {
    return {
      "Accept": "*/*",
      "content-type": "application/json",
      'Authorization': 'Bearer $accessToken',
    };
  }

  List<Vaccine> vaccines = [];

  Future<void> addVax(Vaccine newVax, String accessToken) async {
    final url = Uri.parse(addVaxURL);

    var request = http.MultipartRequest('POST', url)
      ..fields['pet'] = '${newVax.petId}'
      ..fields['vaccine_name'] = newVax.name!
      ..fields['application_date'] =
          '${newVax.applicationDate!.year}-${newVax.applicationDate!.month}-${newVax.applicationDate!.day}'
      ..fields['one_time'] = newVax.oneTime! ? 'True' : 'False'
      ..fields['vax_description'] = newVax.description!
      ..persistentConnection = false;
//this is bad code
    if (!newVax.oneTime!) {
      request.fields['booster_date'] =
          '${newVax.boosterDate!.year}-${newVax.boosterDate!.month}-${newVax.boosterDate!.day}';
    }

    if (newVax.tag != null) {
      request.files.add(
        http.MultipartFile(
          'tag_pic',
          newVax.tag!.readAsBytes().asStream(),
          newVax.tag!.lengthSync(),
          filename: basename(
            newVax.tag!.path,
          ),
        ),
      );
    }

    request.headers.addAll(headers(accessToken));
    var streamedResponse = await request.send();
    var response = await http.Response.fromStream(streamedResponse);
    var decodedResponse = json.decode(response.body);
    if (streamedResponse.statusCode == successfullRequest) {
      final Vaccine addedVax = newVax.copyWith(
        tag: null,
        tagUrl: decodedResponse['tag_pic'],
      );
      vaccines.add(addedVax);
      vaccines.sort(
        (a, b) => a.applicationDate!.compareTo(b.applicationDate!),
      );
    }
    notifyListeners();
  }

  Future<void> getVaxByPet(int petId, String acessToken) async {
    final url = Uri.parse('$getVaxByPetURL$petId/');
    try {
      final response = await http.get(
        url,
        headers: headers(acessToken),
      );
      final List<dynamic> data = json.decode(
        utf8.decode(
          response.bodyBytes,
        ),
      );
      final List<Vaccine> loadedVaccines = [];
      if (response.statusCode == successfullTransaction) {
        for (var newVaccine in data) {
          loadedVaccines.add(
            Vaccine(
              id: newVaccine['id'],
              name: newVaccine['vaccine_name'],
              applicationDate: DateTime.parse(newVaccine['application_date']),
              oneTime: newVaccine['one_time'],
              boosterDate: newVaccine['booster_date'] != null
                  ? DateTime.parse(newVaccine['booster_date']!)
                  : null,
              tagUrl: newVaccine['tag_pic'],
              description: newVaccine['vax_description'],
            ),
          );
        }
      }
      vaccines = loadedVaccines;
      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }

  Future<void> deleteVaccine(Vaccine vaccine, String accessToken) async {
    final int vaccineId = vaccine.id!;
    final url = Uri.parse('$deleteVaccineById$vaccineId/');
    final response = await http.delete(url, headers: headers(accessToken));
    vaccines.remove(vaccine);
    notifyListeners();
  }
}
