import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:pet_vax_app/errors.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
//constants
import '../constans.dart';
//models
import '../models/location_model.dart';
//widgets
import '../widgets/location_dialog.dart'; //Location Dialog widget promted at signup
//Here is where all the validation is handled

class ValidatorProvider with ChangeNotifier {
  //server endpoints
  static const String getCountriesURL = '${serverDomain}get_countries/';
  static const String getStateByCountryURL =
      '${serverDomain}get_states_by_country/';
  static const String getCitiesByStateURL =
      '${serverDomain}get_cities_by_state/';
  //calendar dates setting
  final DateTime firstDate = DateTime(1920);
  final DateTime lastDate = DateTime.now();
  final ImagePicker imagePicker = ImagePicker();
  //for fetching db with list of locations
  List<Country> countryList = [];
  List<GeoState> stateList = [];
  List<City> cityList = [];
  bool isDateConfirmed = false;
  bool isDateValid = true;
  bool isLocationValid = true;
  //user params may not be the best way.
  //one Could store this on sign up states and dispose on widget dispose as vetValue
  String? email;
  String? userPassword;
  String? name;
  String? lastName;
  int? locationId;
  int? gender;
  DateTime selectedDate = DateTime.now();
  File? selectedImage;

  void wipeData() {
    email = null;
    name = null;
    lastName = null;
    selectedDate = DateTime.now();
    userPassword = null;
    isDateConfirmed = false;
    selectedImage = null;
    isDateValid = true;
    isLocationValid = true;
    gender = null;
    locationId = null;
  }

  bool isNotNull(var value) {
    return value != null;
  }

  bool isValidEmail(String email) {
    final emailRegExp = RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    return emailRegExp.hasMatch(email);
  }

  bool isValidName(String name) {
    final nameRegExp = RegExp(r"^\s*([A-Za-z]{1,})$");
    return nameRegExp.hasMatch(name);
  }

  //password must be 8 chars long, include uppercase lowercase an a especial char
  //special chars such as !@#\$&*~._
  bool isValidPassword(String password) {
    final passwordRegExp =
        RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$');
    return passwordRegExp.hasMatch(password);
  }

  //this functions returns an errorcode defined at
  //errors.dart according each error type

  //The oder of each validation seems off
  //Valid should be default output
  int emailValidator(String email) {
    if (!isNotNull(email) || email == '') {
      return isEmpty;
    }
    if (isValidEmail(email)) {
      return isValid;
    }
    return isNotValid;
  }

  int passwordValidator(String password) {
    if (!isNotNull(password) || password == '') {
      return isEmpty;
    }
    if (isValidPassword(password)) {
      return isValid;
    }
    return isNotValid;
  }

  int passwordconfirmValidator(String password) {
    if (password != userPassword) {
      return noMatchPassword;
    }
    return passwordValidator(password);
  }

  int namesValidator(String name) {
    if (!isNotNull(name) || name == '') {
      return isEmpty;
    }
    if (isValidName(name)) {
      return isValid;
    }
    return isNotValid;
  }

  int descriptionValidator(String description) {
    if (!isNotNull(description) || description == '') {
      return isEmpty;
    }
    return isValid;
  }

  int dropDownValidator(int value) {
    if (value == 0) {
      return isNotValid;
    }
    return isValid;
  }

  bool dateValidator() {
    if (isDateConfirmed) {
      isDateValid = true;
    } else {
      isDateValid = false;
    }

    notifyListeners();
    return isDateValid;
  }

  bool locationValidator() {
    if (locationId != null) {
      isLocationValid = true;
    } else {
      isLocationValid = false;
    }
    notifyListeners();
    return isLocationValid;
  }

  bool isSignUpValid(GlobalKey<FormState> formKey) {
    final bool validatedLocation = locationValidator();
    final bool validatedBirthdate = dateValidator();
    bool result = formKey.currentState!.validate() &&
        validatedBirthdate &&
        validatedLocation;
    return result;
  }

  bool isPetFormValid(GlobalKey<FormState> formKey) {
    final bool validatedBirthdate = dateValidator();
    bool result = formKey.currentState!.validate() && validatedBirthdate;
    return result;
  }

  bool isVaxFormValid(GlobalKey<FormState> formKey) {
    final bool validateVaxDate = dateValidator();
    bool result = formKey.currentState!.validate() && validateVaxDate;
    return result;
  }

  Future<void> getCalendar(context) async {
    final ThemeData theme = Theme.of(context);
    await showDatePicker(
      context: context,
      initialDate: selectedDate,
      currentDate: selectedDate,
      firstDate: firstDate,
      lastDate: lastDate,
      builder: (BuildContext context, Widget? child) {
        return Theme(
            data: theme.copyWith(
              colorScheme: ColorScheme.light(
                primary: theme.primaryColor,
                onPrimary: theme.backgroundColor,
              ),
              dialogBackgroundColor: theme.backgroundColor,
            ),
            child: child!);
      },
    ).then(
      (date) => {
        if (date != null)
          {
            selectedDate = date,
            isDateConfirmed = true,
            dateValidator(), //With this call notify listeners gets executed twice :/
            //once at birthdayValidator and once on notifylisteners

            notifyListeners(),
          }
      },
    );
  }

  void selectImage(ImageSource? imageSource)  {
    imagePicker
        .pickImage(
      source: imageSource ?? ImageSource.gallery,
    )
        .then(
      (value) {
        selectedImage = File(
          value!.path,
        );
        notifyListeners();
      },
    );
  }

  //Location management
  void onLocationPressed(context) {
    wipeLocationData();
    listCountries();
    showDialog(
      context: context,
      builder: (context) {
        return const LocationDialog();
      },
    ).then((value) => {locationValidator()});
  }

  Future<void> listCountries() async {
    final url = Uri.parse(getCountriesURL);
    try {
      final response = await http.get(url);
      final List<dynamic> data = json.decode(
        utf8.decode(
          response.bodyBytes,
        ),
      );
      final List<Country> loadedCountryList = [];
      for (var element in data) {
        loadedCountryList.add(
          Country(
            countryId: element['id'],
            countryName: element['country_name'],
          ),
        );
      }
      countryList = loadedCountryList;
      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }

  Future<void> getStatesByCountry(int id) async {
    final int countryId = id;
    final url = Uri.parse('$getStateByCountryURL$countryId/');

    try {
      final response = await http.get(url);
      final List<dynamic> data = json.decode(
        utf8.decode(
          response.bodyBytes,
        ),
      );
      final List<GeoState> loadedStateList = [];
      for (var element in data) {
        loadedStateList.add(
          GeoState(
            stateName: element['state_name'],
            stateId: element['id'],
          ),
        );
        stateList = loadedStateList;
      }
      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }

  Future<void> getCitiesByState(int id) async {
    final int stateId = id;
    final url = Uri.parse('$getCitiesByStateURL$stateId/');

    try {
      final response = await http.get(url);
      final List<dynamic> data = json.decode(
        utf8.decode(
          response.bodyBytes,
        ),
      );
      final List<City> loadedCityList = [];
      for (var element in data) {
        loadedCityList.add(
          City(
            cityName: element['city_name'],
            cityId: element['id'],
          ),
        );
        cityList = loadedCityList;
      }
      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }

  void setLocation(int cityId) {
    locationId = cityId;
  }

  void wipeLocationData() {
    countryList = [];
    stateList = [];
    cityList = [];
    locationId = null;
  }
}
