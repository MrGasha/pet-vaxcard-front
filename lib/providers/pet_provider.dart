import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:path/path.dart'; //for basename
import 'package:http/http.dart' as http;
//Providers
import '../providers/user_provider.dart';
//constans
import '../constans.dart';
//models
import '../models/pet_model.dart';
import '../models/user_model.dart';
import '../models/vax_model.dart';

class PetProvider with ChangeNotifier {
  static const String petsByUserURL = '$serverDomain/get_pets_by_user/';
  static const String registerPetURL = '$serverDomain/register_pet/';
  static const String remindersByUserURL =
      '$serverDomain/get_reminders_by_user/';
  AuthUser authUser = AuthUser(
    accessToken: null,
    refreshToken: null,
  );
  Pet selectedPet = Pet();
  List<Pet> pets = [];
  List<Reminder> reminders = [];
  Map<String, String> get headers {
    return {
      "Accept": "*/*",
      "content-type": "application/json",
      'Authorization': 'Bearer ${authUser.accessToken}',
    };
  }

  void update(UserProvider userProvider) {
    authUser = authUser.copyWith(
      accessToken: userProvider.authUser!.accessToken,
    ); //holding just access token,
  }

  Future<void> getPetsByUser() async {
    final url = Uri.parse(petsByUserURL);
    try {
      final response = await http.get(
        url,
        headers: headers,
      );
      final List<dynamic> data = json.decode(
        utf8.decode(
          response.bodyBytes,
        ),
      );
      final List<Pet> loadedPets = [];
      if (response.statusCode == successfullTransaction) {
        for (var element in data) {
          loadedPets.add(
            Pet(
              name: element['pet_name'],
              id: element['id'],
              speciesId: element['species'],
              genderId: element['gender'],
              picUrl: element['pet_pic'],
              description: element['pet_description'],
              birthdate: DateTime.parse(element['pet_birthdate']),
            ),
          );
        }
      }
      pets = loadedPets;
      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }

  Future<void> registerPet(Pet newPet) async {
    final url = Uri.parse(registerPetURL);
    var request = http.MultipartRequest('POST', url)
      ..fields['pet_name'] = newPet.name!
      ..fields['pet_birthdate'] =
          '${newPet.birthdate!.year}-${newPet.birthdate!.month}-${newPet.birthdate!.day}'
      ..fields['species'] = '${newPet.speciesId}'
      ..fields['gender'] = '${newPet.genderId}'
      ..fields['pet_description'] = newPet.description!
      ..persistentConnection = false;

    if (newPet.pic != null) {
      request.files.add(
        http.MultipartFile(
          'pet_pic',
          newPet.pic!.readAsBytes().asStream(),
          newPet.pic!.lengthSync(),
          filename: basename(
            newPet.pic!.path,
          ),
        ),
      );
    }
    request.headers.addAll(headers);
    var streamedResponse = await request.send();
    var response = await http.Response.fromStream(streamedResponse);
    var decodedResponse = json.decode(response.body);
    if (streamedResponse.statusCode == successfullRequest) {
      final Pet registeredPet = newPet.copyWith(
        pic: null,
        picUrl: decodedResponse['pet_pic'],
        id: decodedResponse['id'],
      );
      pets.add(registeredPet);
    }
    notifyListeners();
  }

  Future<void> getRemindersByUser() async {
    final url = Uri.parse(remindersByUserURL);
    try {
      final response = await http.get(
        url,
        headers: headers,
      );
      final List<dynamic> data = json.decode(
        utf8.decode(
          response.bodyBytes,
        ),
      );
      final List<Reminder> loadedReminders = [];
      if (response.statusCode == successfullTransaction) {
        for (var element in data) {
          final newReminder = Reminder(
            vaccine: Vaccine(
              name: element['vaccine']['vaccine_name'],
              applicationDate: DateTime.parse(
                element['vaccine']['application_date'],
              ),
              boosterDate: DateTime.parse(
                element['vaccine']['booster_date'],
              ),
              id: element['vaccine']['id'],
            ),
            reminderType: element['reminder_type'],
            petName: element['vaccine']['pet']['pet_name'],
          );
          loadedReminders.add(
            newReminder,
          );
        }
        reminders = loadedReminders;
        notifyListeners();
      }
    } catch (error) {
      rethrow;
    }
  }
}
