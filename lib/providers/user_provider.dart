import 'dart:convert';

import 'package:path/path.dart'; //for basename
import 'package:flutter/material.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:http/http.dart' as http;
//models
import '../models/user_model.dart';
//constants
import '../constans.dart';

class UserProvider with ChangeNotifier {
  static const String signUpURL = '${serverDomain}signup/';
  static const String loginURL = '${serverDomain}api/token/';
  static const String refreshTokenURL = '${serverDomain}api/token/refresh/';
  static const String getUserByIdURL = '${serverDomain}get_user/';
  static const String registerVet = '${serverDomain}register_vet/';
  Map<String, String> get headers {
    return {
      "Accept": "*/*",
      "content-type": "application/json",
      'Authorization': 'Bearer ${authUser!.accessToken}',
    };
  }

  User? user;
  AuthUser? authUser;

  Future<bool> get isAuth async {
    return await accessToken != null;
  }

  Future<String?> get accessToken async {
    if (authUser != null) {
      if (JwtDecoder.isExpired(authUser!.accessToken!)) {
        return await newAccessToken; //if access token expired request new
      }
      return authUser!.accessToken; // if not expired return current
    }
    return null; //if auth user null, is not logged nor auth
  }

  Future<String?> get newAccessToken async {
    if (JwtDecoder.isExpired(authUser!.refreshToken!)) {
      return null;
    } else {
      return await refreshAccessToken();
    }
  }

  Future<String?> refreshAccessToken() async {
    final url = Uri.parse(refreshTokenURL);
    try {
      final response = await http.post(
        url,
        headers: {
          "Accept": "*/*",
          "content-type": "application/json",
        },
        body: json.encode(
          {
            "refresh": authUser!.refreshToken!,
          },
        ),
      );
      final decodedResponse = json.decode(response.body);
      if (response.statusCode == successfullTransaction) {
        authUser = authUser!.copyWith(
          accessToken: decodedResponse['access'],
        );
      } else {
        authUser = null;
      }
      notifyListeners();
      return authUser?.accessToken;
    } catch (error) {
      rethrow;
    }
  }

  Future<void> signUp(User newUser, BuildContext context) async {
    final url = Uri.parse(signUpURL);
    var request = http.MultipartRequest('POST', url)
      ..fields['email'] = newUser.email!
      ..fields['password'] = newUser.password!
      ..fields['name'] = newUser.name!
      ..fields['last_name'] = newUser.lastName!
      ..fields['role'] = newUser.isVet! ? vet : owner
      ..fields['gender'] = '${newUser.gender}'
      ..fields['birthdate'] =
          '${newUser.birthdate!.year}-${newUser.birthdate!.month}-${newUser.birthdate!.day}'
      ..fields['location'] = '${newUser.locationId}'
      ..persistentConnection = false;
    if (newUser.profilePic != null) {
      request.files.add(
        http.MultipartFile(
          'profile_picture',
          newUser.profilePic!.readAsBytes().asStream(),
          newUser.profilePic!.lengthSync(),
          filename: basename(
            newUser.profilePic!.path,
          ),
        ),
      );
    }

    var streamedResponse = await request.send();
    var response = await http.Response.fromStream(streamedResponse);
    var decodedResponse = json.decode(response.body);
    if (streamedResponse.statusCode == successfullRequest) {
      //log the user, give feedback of the transaction,
      user = newUser;

      await logIn(
        user!.email!,
        user!.password!,
      ).then(
        (value) {
          if (newUser.isVet!) {
            final url = Uri.parse(registerVet);
            http.post(
              url,
              headers: headers,
              body: json.encode(
                {
                  "user": authUser?.userId!,
                },
              ),
            );
          }
        },
      );
      user = user!.copyWith(password: decodedResponse['password']);
      //WHY AM I STORING HASHED PASSWORD CANNOT REMEMBER WTF xd
    } //aproved transaction
    //if transaction denied, give a reason why did not passed.
  }

  Future<void> logIn(String email, String password) async {
    final url = Uri.parse(loginURL);
    try {
      var response = await http.post(
        url,
        headers: {
          "Accept": "*/*",
          "content-type": "application/json",
        },
        body: json.encode(
          {
            "email": email,
            "password": password,
          },
        ),
      );
      final decodedResponse = json.decode(response.body);
      if (response.statusCode == successfullTransaction) {
        final AuthUser loggedUser = AuthUser(
          accessToken: decodedResponse['access'],
          refreshToken: decodedResponse['refresh'],
        );

        final int userId =
            JwtDecoder.decode(loggedUser.accessToken!)['user_id'];
        authUser = loggedUser.copyWith(userId: userId);
      }
    } catch (error) {
      rethrow;
    }

    notifyListeners();
  }

  Future<void> getUserById() async {
    final url = Uri.parse(getUserByIdURL);
    try {
      final response = await http.get(
        url,
        headers: headers,
      );
      if (response.statusCode == successfullTransaction) {
        final Map<String, dynamic> data =
            json.decode(utf8.decode(response.bodyBytes));

        final User userProfile = User(
          name: data['name'],
          lastName: data['last_name'],
          profilePicURL: data['profile_picture'],
        );
        user = userProfile;
      }
      notifyListeners();
      //deletes unnecesary info and holds on info to build profile
    } catch (error) {
      rethrow;
    }
  }
}
