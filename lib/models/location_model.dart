class Country {
  final String countryName;
  final int countryId;
  const Country({
    required this.countryId,
    required this.countryName,
  });
}

class GeoState {
  final String stateName;
  final int stateId;
  const GeoState({
    required this.stateName,
    required this.stateId,
  });
}

class City {
  final String cityName;
  final int cityId;
  const City({
    required this.cityName,
    required this.cityId,
  });
}
