import 'dart:io';

class User {
  final String? email;
  final String? password;
  final String? name;
  final String? lastName;
  final File? profilePic;
  final String? profilePicURL;
  final int? gender;
  final int? locationId;
  final DateTime? birthdate;
  final bool? isVet;
  User copyWith({
    String? email,
    String? password,
    String? name,
    String? lastName,
    File? profilePic,
    String? profilePicURL,
    int? gender,
    int? locationId,
    DateTime? birthdate,
    bool? isVet,
  }) {
    return User(
      email: email ?? this.email,
      password: password ?? this.password,
      name: name ?? this.name,
      lastName: lastName ?? this.lastName,
      gender: gender ?? this.gender,
      birthdate: birthdate ?? this.birthdate,
      profilePic: profilePic ?? this.profilePic,
      profilePicURL: profilePicURL ?? this.profilePicURL,
      locationId: locationId ?? this.locationId,
      isVet: isVet ?? this.isVet,
    );
  }

  User({
    this.email,
    this.password,
    this.name,
    this.lastName,
    this.gender,
    this.locationId,
    this.birthdate,
    this.profilePic,
    this.profilePicURL,
    this.isVet,
  });
}

class AuthUser {
  final int? userId;
  final String? accessToken;
  final String? refreshToken;
  AuthUser copyWith({
    int? userId,
    String? accessToken,
    String? refreshToken,
  }) {
    return AuthUser(
      userId: userId ?? this.userId,
      accessToken: accessToken ?? this.accessToken,
      refreshToken: refreshToken ?? this.refreshToken,
    );
  }

  AuthUser({
    this.userId,
    required this.accessToken,
    required this.refreshToken,
  });
}
