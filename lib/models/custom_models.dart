import 'package:flutter/material.dart';

class SearchDropDownItem {
  final String itemName;
  final int itemId;
  const SearchDropDownItem({
    required this.itemName,
    required this.itemId,
  });
}

class TabItem {
  final Widget tab;
  final int tabId;
  bool isSelected;
  TabItem({
    required this.tab,
    required this.tabId,
    this.isSelected = false,
  });
}
