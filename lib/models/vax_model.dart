import 'dart:io';
import 'package:pdf/widgets.dart' as pw;

class Vaccine {
  final int? id;
  final int? petId;
  final String? name;
  final String? description;
  final bool? oneTime;
  final int? vetId;
  final int? periodicity;
  final File? tag;
  final String? tagUrl;
  final pw.MemoryImage? tagPdf;
  final DateTime? applicationDate;
  final DateTime? boosterDate;

  Vaccine copyWith({
    int? id,
    int? petId,
    String? name,
    String? description,
    bool? oneTime,
    int? vetId,
    int? periodicity,
    File? tag,
    String? tagUrl,
    pw.MemoryImage? tagPdf,
    DateTime? applicationDate,
    DateTime? boosterDate,
  }) {
    return Vaccine(
      id: id ?? this.id,
      petId: petId ?? this.petId,
      name: name ?? this.name,
      description: description ?? this.description,
      oneTime: oneTime ?? this.oneTime,
      vetId: vetId ?? this.vetId,
      periodicity: periodicity ?? this.periodicity,
      tag: tag ?? this.tag,
      tagUrl: tagUrl ?? this.tagUrl,
      tagPdf: tagPdf ?? this.tagPdf,
      applicationDate: applicationDate ?? this.applicationDate,
      boosterDate: boosterDate ?? this.boosterDate,
    );
  }

  Vaccine({
    this.id,
    this.petId,
    this.name,
    this.description,
    this.oneTime,
    this.vetId,
    this.periodicity,
    this.tag,
    this.tagUrl,
    this.tagPdf,
    this.applicationDate,
    this.boosterDate,
  });
}
