import 'dart:io';
import './vax_model.dart';

class Pet {
  final String? name;
  final int? id;
  final String? species;
  final String? gender;
  final int? speciesId;
  final int? genderId;
  final String? description;
  final File? pic;
  final String? picUrl;
  final DateTime? birthdate;
  Pet copyWith({
    String? name,
    int? id,
    int? genderId,
    int? speciesId,
    String? species,
    String? gender,
    String? description,
    File? pic,
    String? picUrl,
    DateTime? birthdate,
  }) {
    return Pet(
      name: name ?? this.name,
      id: id ?? this.id,
      speciesId: speciesId ?? this.speciesId,
      genderId: genderId ?? this.genderId,
      species: species ?? this.species,
      gender: gender ?? this.gender,
      description: description ?? this.description,
      pic: pic ?? this.pic,
      picUrl: picUrl ?? this.picUrl,
      birthdate: birthdate ?? this.birthdate,
    );
  }

  Pet({
    this.name,
    this.id,
    this.genderId,
    this.speciesId,
    this.description,
    this.species,
    this.gender,
    this.pic,
    this.picUrl,
    this.birthdate,
  });
}

class Reminder {
  final Vaccine? vaccine;
  final String? petName;
  final int? reminderType;
  Reminder copyWith({
    Vaccine? vaccine,
    String? petName,
    int? reminderType,
  }) {
    return Reminder(
      vaccine: vaccine ?? this.vaccine,
      petName: petName ?? this.petName,
      reminderType: reminderType ?? this.reminderType,
    );
  }

  Reminder({
    this.vaccine,
    this.petName,
    this.reminderType,
  });
}
