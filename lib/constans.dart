import 'package:flutter/material.dart';
import 'package:pdf/pdf.dart';

const Color primaryColor = Color(0xFFEA907A);

//light Theme
const Color bgColorLightTheme = Color(0xFFF4F7C5);
const Color secondaryColorLightTheme = Color(0xFFFBC687);
const Color auxColorLightTheme = Color(0xFFAACDBE);
//pdf colors
const PdfColor pdfBackground =
    PdfColor.fromInt(0xFFFBC687); //Secondary color as bg
const PdfColor pdfPrimary = PdfColor.fromInt(0xFFF4F7C5); //bgColorformConstants
const PdfColor pdfTextPrimaryColor = PdfColor.fromInt(0xFFEA907A);
//defaults

const double defaultPadding = 15.0;
const double defaultElevatedButtonPadding = 20.0;
const double defaultIconSplashRadius = 20.0;
const double defaultBorderRadius = 5.0;
const double modalBorderRadius = 4 * defaultBorderRadius;
const double dialogBorderRadius = 3 * defaultBorderRadius;
const double tabButtonBorderRadius = 20;
const double checkBoxDefaultwidth = 2.0;
const double defaultSpashRadiusCheckBox = 15.0;
//user vet codes
const String owner = '0';
const String vet = '1';
//reminder codes
const int reminder24H = 1;
const int reminder48H = 2;
const int reminder1W = 3;
const int reminder1M = 4;
const int reminder3M = 5;

//status codes
const int successfullRequest = 201;
const int successfullTransaction = 200;
//Local environment for tests
const String serverDomain = 'http://192.168.1.89:8000/';
